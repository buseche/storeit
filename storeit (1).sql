-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 22-07-2020 a las 11:52:38
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `storeit`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_almacen_entradas`
--

CREATE TABLE `sta_almacen_entradas` (
  `numregent` int(11) NOT NULL,
  `numfac` varchar(20) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `provid` int(11) NOT NULL,
  `fecfac` date NOT NULL,
  `fecent` date NOT NULL,
  `usuregent` int(11) NOT NULL,
  `entcoment` varchar(512) COLLATE utf8mb4_spanish2_ci DEFAULT NULL COMMENT 'Comentario adicional'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci COMMENT='Tabla de almacenaje de los items del almacen';

--
-- Volcado de datos para la tabla `sta_almacen_entradas`
--

INSERT INTO `sta_almacen_entradas` (`numregent`, `numfac`, `provid`, `fecfac`, `fecent`, `usuregent`, `entcoment`) VALUES
(1, '54785', 4, '2020-07-17', '2020-07-21', 2, 'Carga de prueba'),
(2, '625478', 4, '2020-07-13', '2020-07-21', 2, 'Carga de prueba 2'),
(3, '65477', 4, '2020-06-03', '2020-07-21', 2, 'test 3'),
(4, '32547', 4, '2020-07-16', '2020-07-21', 2, 'tst 3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_almacen_salidas`
--

CREATE TABLE `sta_almacen_salidas` (
  `salidaid` int(11) NOT NULL,
  `fecsal` date NOT NULL,
  `numorden` int(11) NOT NULL,
  `depdest` int(11) NOT NULL,
  `usureg` int(11) NOT NULL,
  `statusid` int(11) NOT NULL,
  `commsal` varchar(512) COLLATE utf8mb4_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci COMMENT='tabla de registros de salidas';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_departamentos`
--

CREATE TABLE `sta_departamentos` (
  `deptid` int(11) NOT NULL,
  `depnom` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `sta_departamentos`
--

INSERT INTO `sta_departamentos` (`deptid`, `depnom`) VALUES
(1, 'Desarrollo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_dep_dir`
--

CREATE TABLE `sta_dep_dir` (
  `unionid` int(11) NOT NULL,
  `depid` int(11) NOT NULL,
  `dirid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `sta_dep_dir`
--

INSERT INTO `sta_dep_dir` (`unionid`, `depid`, `dirid`) VALUES
(1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_detalles_ordenes`
--

CREATE TABLE `sta_detalles_ordenes` (
  `itemid` int(11) NOT NULL,
  `numorden` int(11) NOT NULL,
  `codbar` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `numuniap` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_detalles_preordenes`
--

CREATE TABLE `sta_detalles_preordenes` (
  `detalleid` int(11) NOT NULL,
  `presprod` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `numorden` int(11) NOT NULL,
  `codbar` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `numuni` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_direcciones`
--

CREATE TABLE `sta_direcciones` (
  `dirid` int(11) NOT NULL,
  `dirnom` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `sta_direcciones`
--

INSERT INTO `sta_direcciones` (`dirid`, `dirnom`) VALUES
(1, 'Direccion de Tecnologia y Sistemas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_entradas_detalles`
--

CREATE TABLE `sta_entradas_detalles` (
  `itemid` int(11) NOT NULL,
  `regent` int(11) NOT NULL,
  `prodpresent` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `numunid` int(11) NOT NULL,
  `costuni` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `codbar` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `sta_entradas_detalles`
--

INSERT INTO `sta_entradas_detalles` (`itemid`, `regent`, `prodpresent`, `numunid`, `costuni`, `codbar`) VALUES
(21, 1, 'Caja', 280, '350000', '5010000091'),
(22, 1, 'Caja', 500, '112358', '7591447360022'),
(23, 1, 'Caja', 2400, '56825', '7750082039209'),
(24, 2, 'Caja', 300, '115326', '9788466616843'),
(25, 2, 'Caja', 200, '112358', '9789803810887'),
(26, 2, 'Caja', 300, '56825', '9788497595308'),
(27, 3, 'Caja', 300, '112358', '9788401422812'),
(28, 4, 'Caja', 200, '112358', '9788497595308'),
(29, 4, 'Caja', 320, '656478.25', '9789803810887');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_existencias`
--

CREATE TABLE `sta_existencias` (
  `itemid` int(11) NOT NULL,
  `codbar` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `numexis` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `sta_existencias`
--

INSERT INTO `sta_existencias` (`itemid`, `codbar`, `numexis`) VALUES
(7, '5010000091', 280),
(8, '7591447360022', 500),
(9, '7750082039209', 2400),
(10, '9788466616843', 300),
(11, '9789803810887', 520),
(12, '9788497595308', 500),
(13, '9788401422812', 300);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_log`
--

CREATE TABLE `sta_log` (
  `id` int(11) NOT NULL,
  `access_ip` varchar(15) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `url_request` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `data_request` longtext COLLATE utf8mb4_spanish2_ci DEFAULT NULL
) ;

--
-- Volcado de datos para la tabla `sta_log`
--

INSERT INTO `sta_log` (`id`, `access_ip`, `url_request`, `data_request`, `user_agent`, `access_date`, `user_access`) VALUES
(1, '::1', 'inicio', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:36:56', NULL),
(2, '::1', '/', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:36:57', NULL),
(3, '::1', 'signin', '{\"usuemail\":\"bryan.useche@sapi.gob.ve\",\"usupass\":\"r0ckb@nd15\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:39:18', NULL),
(4, '::1', 'inicio', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:39:19', 'bryan.useche@sapi.gob.ve'),
(5, '::1', 'entradas', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:39:50', 'bryan.useche@sapi.gob.ve'),
(6, '::1', 'entradas', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:41:04', 'bryan.useche@sapi.gob.ve'),
(7, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:48:10', 'bryan.useche@sapi.gob.ve'),
(8, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:49:08', 'bryan.useche@sapi.gob.ve'),
(9, '::1', 'addentrada', '{\"numfac\":\"000001\",\"fecfac\":\"2020-07-20\",\"provid\":\"4\",\"fecent\":\"2020-07-20\",\"entcoment\":\"Carga de Prueba\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:49:18', 'bryan.useche@sapi.gob.ve'),
(10, '::1', 'buscarxcodbar', '{\"data\":\"5010000091\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:49:39', 'bryan.useche@sapi.gob.ve'),
(11, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:51:44', 'bryan.useche@sapi.gob.ve'),
(12, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:52:12', 'bryan.useche@sapi.gob.ve'),
(13, '::1', 'addentrada', '{\"numfac\":\"000001\",\"fecfac\":\"2020-07-17\",\"provid\":\"4\",\"fecent\":\"2020-07-20\",\"entcoment\":\"Carga de prueba\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:52:21', 'bryan.useche@sapi.gob.ve'),
(14, '::1', 'buscarxcodbar', '{\"data\":\"5010000091\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 16:52:36', 'bryan.useche@sapi.gob.ve'),
(15, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:30:08', 'bryan.useche@sapi.gob.ve'),
(16, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:30:57', 'bryan.useche@sapi.gob.ve'),
(17, '::1', 'addentrada', '{\"numfac\":\"000001\",\"fecfac\":\"2020-07-17\",\"provid\":\"4\",\"fecent\":\"2020-07-20\",\"entcoment\":\"Carga de Prueba\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:31:06', 'bryan.useche@sapi.gob.ve'),
(18, '::1', 'buscarxcodbar', '{\"data\":\"5010000091\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:31:26', 'bryan.useche@sapi.gob.ve'),
(19, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:32:09', 'bryan.useche@sapi.gob.ve'),
(20, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:32:59', 'bryan.useche@sapi.gob.ve'),
(21, '::1', 'addentrada', '{\"numfac\":\"000001\",\"fecfac\":\"2020-07-17\",\"provid\":\"4\",\"fecent\":\"2020-07-20\",\"entcoment\":\"Carga de prueba\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:33:07', 'bryan.useche@sapi.gob.ve'),
(22, '::1', 'buscarxcodbar', '{\"data\":\"5010000091\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:34:01', 'bryan.useche@sapi.gob.ve'),
(23, '::1', 'adddetalle', '{\"codbar\":\"5010000091\",\"regent\":\"1\",\"costuni\":\"200000.00\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:40:52', 'bryan.useche@sapi.gob.ve'),
(24, '::1', 'adddetalle', '{\"codbar\":\"5010000091\",\"regent\":\"1\",\"costuni\":\"200000.00\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:41:19', 'bryan.useche@sapi.gob.ve'),
(25, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:43:59', 'bryan.useche@sapi.gob.ve'),
(26, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:48:50', 'bryan.useche@sapi.gob.ve'),
(27, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:49:39', 'bryan.useche@sapi.gob.ve'),
(28, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:49:57', 'bryan.useche@sapi.gob.ve'),
(29, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:50:55', 'bryan.useche@sapi.gob.ve'),
(30, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:51:24', 'bryan.useche@sapi.gob.ve'),
(31, '::1', 'addentrada', '{\"numfac\":\"000001\",\"fecfac\":\"2020-07-17\",\"provid\":\"4\",\"fecent\":\"2020-07-20\",\"entcoment\":\"Carga de prueba\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:51:33', 'bryan.useche@sapi.gob.ve'),
(32, '::1', 'buscarxcodbar', '{\"data\":\"5010000091\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:51:55', 'bryan.useche@sapi.gob.ve'),
(33, '::1', 'adddetalle', '{\"codbar\":\"5010000091\",\"regent\":\"1\",\"numunid\":\"50\",\"costuni\":\"200000.00\",\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 05:52:06', 'bryan.useche@sapi.gob.ve'),
(34, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:15:32', 'bryan.useche@sapi.gob.ve'),
(35, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:17:30', 'bryan.useche@sapi.gob.ve'),
(36, '::1', 'buscarproveedor', '{\"rif\":\"J000202001\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:18:23', 'bryan.useche@sapi.gob.ve'),
(37, '::1', 'nuevoproveedor', '{\"numrif\":\"J000202001\",\"nomprov\":\"Farmatodo C.A\",\"direccprov\":\"Av Los Guayabitos, CC Expreso Baruta, Nivel 5, Oficina Unica. Urb La Trinidad, Sector Piedra azul, Caracas\",\"telef1\":\"0800327628636\",\"telef2\":\"02128306870\",\"email\":\"alguien@microsoft.com\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:20:44', 'bryan.useche@sapi.gob.ve'),
(38, '::1', 'buscarproveedor', '{\"rif\":\"J000202001\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:20:45', 'bryan.useche@sapi.gob.ve'),
(39, '::1', 'addentrada', '{\"numfac\":\"28202\",\"fecfac\":\"2020-06-16\",\"provid\":\"5\",\"fecent\":\"2020-07-20\",\"entcoment\":\"Carga de prueba 2\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:21:40', 'bryan.useche@sapi.gob.ve'),
(40, '::1', 'buscarxcodbar', '{\"data\":\"088591200061\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:22:01', 'bryan.useche@sapi.gob.ve'),
(41, '::1', 'addproduct', '{\"codbar\":\"088591200061\",\"prodmar\":\"Netis\",\"prodmodel\":\"Switch Ethernet 5 Puertos\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:22:35', 'bryan.useche@sapi.gob.ve'),
(42, '::1', 'adddetalle', '{\"codbar\":\"088591200061\",\"regent\":\"1\",\"numunid\":\"40\",\"costuni\":2198278,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:23:20', 'bryan.useche@sapi.gob.ve'),
(43, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:23:21', 'bryan.useche@sapi.gob.ve'),
(44, '::1', 'buscarxcodbar', '{\"data\":\"9786071503077\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:23:46', 'bryan.useche@sapi.gob.ve'),
(45, '::1', 'addproduct', '{\"codbar\":\"9786071503077\",\"prodmar\":\"McGrawHill Educacion\",\"prodmodel\":\"Libro Chang Quimica 10? Edicion\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:24:33', 'bryan.useche@sapi.gob.ve'),
(46, '::1', 'addproduct', '{\"codbar\":\"9786071503077\",\"prodmar\":\"McGrawHill Educacion\",\"prodmodel\":\"Libro Chang Quimica 10? Edicion\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:26:16', 'bryan.useche@sapi.gob.ve'),
(47, '::1', 'adddetalle', '{\"codbar\":\"9786071503077\",\"regent\":\"1\",\"numunid\":\"60\",\"costuni\":704000,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:26:34', 'bryan.useche@sapi.gob.ve'),
(48, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:26:34', 'bryan.useche@sapi.gob.ve'),
(49, '::1', 'buscarxcodbar', '{\"data\":\"9789505470655\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:27:05', 'bryan.useche@sapi.gob.ve'),
(50, '::1', 'addproduct', '{\"codbar\":\"9789505470655\",\"prodmar\":\"Minotauro\",\"prodmodel\":\"Libro El Se?or de los Anillos II. Las Dos Torres\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:27:59', 'bryan.useche@sapi.gob.ve'),
(51, '::1', 'adddetalle', '{\"codbar\":\"9789505470655\",\"regent\":\"1\",\"numunid\":\"45\",\"costuni\":97103,\"prodpresent\":\"Bulto\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:28:33', 'bryan.useche@sapi.gob.ve'),
(52, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:28:34', 'bryan.useche@sapi.gob.ve'),
(53, '::1', 'buscarxcodbar', '{\"data\":\"97899505470662\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:30:06', 'bryan.useche@sapi.gob.ve'),
(54, '::1', 'addproduct', '{\"codbar\":\"97899505470662\",\"prodmar\":\"Minotauro\",\"prodmodel\":\"Libro El Se?or de los Anillos III. El Retorno del Rey\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:30:36', 'bryan.useche@sapi.gob.ve'),
(55, '::1', 'adddetalle', '{\"codbar\":\"97899505470662\",\"regent\":\"1\",\"numunid\":\"60\",\"costuni\":387069,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:31:01', 'bryan.useche@sapi.gob.ve'),
(56, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:31:02', 'bryan.useche@sapi.gob.ve'),
(57, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:46:21', 'bryan.useche@sapi.gob.ve'),
(58, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:48:08', 'bryan.useche@sapi.gob.ve'),
(59, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:50:11', 'bryan.useche@sapi.gob.ve'),
(60, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:51:02', 'bryan.useche@sapi.gob.ve'),
(61, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:52:26', 'bryan.useche@sapi.gob.ve'),
(62, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:53:59', 'bryan.useche@sapi.gob.ve'),
(63, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:54:22', 'bryan.useche@sapi.gob.ve'),
(64, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 06:55:24', 'bryan.useche@sapi.gob.ve'),
(65, '::1', 'entradas', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:21:32', 'bryan.useche@sapi.gob.ve'),
(66, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:22:01', 'bryan.useche@sapi.gob.ve'),
(67, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:23:27', 'bryan.useche@sapi.gob.ve'),
(68, '::1', 'addentrada', '{\"numfac\":\"00002\",\"fecfac\":\"2020-07-20\",\"provid\":\"4\",\"fecent\":\"2020-07-20\",\"entcoment\":\"Carga de prueba en existencias\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:23:38', 'bryan.useche@sapi.gob.ve'),
(69, '::1', 'buscarxcodbar', '{\"data\":\"5010000091\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:23:55', 'bryan.useche@sapi.gob.ve'),
(70, '::1', 'adddetalle', '{\"codbar\":\"5010000091\",\"regent\":\"2\",\"numunid\":\"50\",\"costuni\":230000,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:24:05', 'bryan.useche@sapi.gob.ve'),
(71, '::1', 'detalles', '{\"numregent\":\"2\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:24:06', 'bryan.useche@sapi.gob.ve'),
(72, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:26:50', 'bryan.useche@sapi.gob.ve'),
(73, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:32:52', 'bryan.useche@sapi.gob.ve'),
(74, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:34:50', 'bryan.useche@sapi.gob.ve'),
(75, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:35:11', 'bryan.useche@sapi.gob.ve'),
(76, '::1', 'addentrada', '{\"numfac\":\"547894\",\"fecfac\":\"2020-07-20\",\"provid\":\"4\",\"fecent\":\"2020-07-20\",\"entcoment\":\"Carga de prueba 3\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:35:20', 'bryan.useche@sapi.gob.ve'),
(77, '::1', 'buscarxcodbar', '{\"data\":\"9788476728055\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:35:38', 'bryan.useche@sapi.gob.ve'),
(78, '::1', 'addproduct', '{\"codbar\":\"9788476728055\",\"prodmar\":\"Fontana\",\"prodmodel\":\"Libro Robinson Crusoe, Daniel Defoe\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:36:11', 'bryan.useche@sapi.gob.ve'),
(79, '::1', 'adddetalle', '{\"codbar\":\"9788476728055\",\"regent\":\"1\",\"numunid\":\"50\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:36:25', 'bryan.useche@sapi.gob.ve'),
(80, '::1', 'adddetalle', '{\"codbar\":\"9788476728055\",\"regent\":\"1\",\"numunid\":\"50\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:37:58', 'bryan.useche@sapi.gob.ve'),
(81, '::1', 'adddetalle', '{\"codbar\":\"9788476728055\",\"regent\":\"1\",\"numunid\":\"50\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:39:13', 'bryan.useche@sapi.gob.ve'),
(82, '::1', 'adddetalle', '{\"codbar\":\"9788476728055\",\"regent\":\"1\",\"numunid\":\"50\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:40:38', 'bryan.useche@sapi.gob.ve'),
(83, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:40:39', 'bryan.useche@sapi.gob.ve'),
(84, '::1', 'buscarxcodbar', '{\"data\":\"9789505470655\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:44:19', 'bryan.useche@sapi.gob.ve'),
(85, '::1', 'buscarxcodbar', '{\"data\":\"9788476728055\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:44:39', 'bryan.useche@sapi.gob.ve'),
(86, '::1', 'adddetalle', '{\"codbar\":\"9788476728055\",\"regent\":\"1\",\"numunid\":\"50\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:44:46', 'bryan.useche@sapi.gob.ve'),
(87, '::1', 'adddetalle', '{\"codbar\":\"9788476728055\",\"regent\":\"1\",\"numunid\":\"50\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:49:27', 'bryan.useche@sapi.gob.ve'),
(88, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:49:28', 'bryan.useche@sapi.gob.ve'),
(89, '::1', 'entradas', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:55:45', 'bryan.useche@sapi.gob.ve'),
(90, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:56:03', 'bryan.useche@sapi.gob.ve'),
(91, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:56:30', 'bryan.useche@sapi.gob.ve'),
(92, '::1', 'addentrada', '{\"numfac\":\"584789\",\"fecfac\":\"2020-07-20\",\"provid\":\"4\",\"fecent\":\"2020-07-20\",\"entcoment\":\"Carga de prueba\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:56:41', 'bryan.useche@sapi.gob.ve'),
(93, '::1', 'buscarxcodbar', '{\"data\":\"9789505470662\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:56:59', 'bryan.useche@sapi.gob.ve'),
(94, '::1', 'addproduct', '{\"codbar\":\"9789505470662\",\"prodmar\":\"Minotauro\",\"prodmodel\":\"Libro El Se?or de los Anillos III. El Retorno del Rey\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:57:08', 'bryan.useche@sapi.gob.ve'),
(95, '::1', 'adddetalle', '{\"codbar\":\"9789505470662\",\"regent\":\"1\",\"numunid\":\"50\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:57:19', 'bryan.useche@sapi.gob.ve'),
(96, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:57:20', 'bryan.useche@sapi.gob.ve'),
(97, '::1', 'buscarxcodbar', '{\"data\":\"9789505470655\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:59:00', 'bryan.useche@sapi.gob.ve'),
(98, '::1', 'addproduct', '{\"codbar\":\"9789505470655\",\"prodmar\":\"Minotauro\",\"prodmodel\":\"Libro El Se?or de los Anillos II. Las Dos Torres\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:59:08', 'bryan.useche@sapi.gob.ve'),
(99, '::1', 'adddetalle', '{\"codbar\":\"9789505470655\",\"regent\":\"1\",\"numunid\":\"50\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:59:14', 'bryan.useche@sapi.gob.ve'),
(100, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:59:15', 'bryan.useche@sapi.gob.ve'),
(101, '::1', 'buscarxcodbar', '{\"data\":\"9788497632003\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 07:59:36', 'bryan.useche@sapi.gob.ve'),
(102, '::1', 'addproduct', '{\"codbar\":\"9788497632003\",\"prodmar\":\"Nowtilus Frontera\",\"prodmodel\":\"Libro NeoNazis, la seducci?n de la sv?stica, Antonio Luis Moyano\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 08:00:23', 'bryan.useche@sapi.gob.ve'),
(103, '::1', 'adddetalle', '{\"codbar\":\"9788497632003\",\"regent\":\"1\",\"numunid\":\"100\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 08:00:34', 'bryan.useche@sapi.gob.ve'),
(104, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 08:00:34', 'bryan.useche@sapi.gob.ve'),
(105, '::1', 'buscarxcodbar', '{\"data\":\"9788497595308\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 08:01:00', 'bryan.useche@sapi.gob.ve'),
(106, '::1', 'addproduct', '{\"codbar\":\"9788497595308\",\"prodmar\":\"DeBolsillo\",\"prodmodel\":\"Libro La Isla de las Tormentas, Ken Follet\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 08:01:25', 'bryan.useche@sapi.gob.ve'),
(107, '::1', 'adddetalle', '{\"codbar\":\"9788497595308\",\"regent\":\"1\",\"numunid\":\"300\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 08:01:33', 'bryan.useche@sapi.gob.ve'),
(108, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 08:01:34', 'bryan.useche@sapi.gob.ve'),
(109, '::1', 'entradas', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-20 08:01:56', 'bryan.useche@sapi.gob.ve'),
(110, '::1', '/', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:28:02', NULL),
(111, '::1', 'signin', '{\"usuemail\":\"bryan.useche@sapi.gob.ve\",\"usupass\":\"r0ckb@nd15\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:29:36', NULL),
(112, '::1', 'inicio', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:29:38', 'bryan.useche@sapi.gob.ve'),
(113, '::1', 'entradas', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:49:01', 'bryan.useche@sapi.gob.ve'),
(114, '::1', 'entradas', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:49:44', 'bryan.useche@sapi.gob.ve'),
(115, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:49:52', 'bryan.useche@sapi.gob.ve'),
(116, '::1', 'buscarproveedor', '{\"rif\":\"J000000000\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:50:15', 'bryan.useche@sapi.gob.ve'),
(117, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:52:26', 'bryan.useche@sapi.gob.ve'),
(118, '::1', 'buscarproveedor', '{\"rif\":\"J000000020\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:52:43', 'bryan.useche@sapi.gob.ve'),
(119, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:53:14', 'bryan.useche@sapi.gob.ve'),
(120, '::1', 'buscarproveedor', '{\"rif\":\"J000000000\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:53:42', 'bryan.useche@sapi.gob.ve'),
(121, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:54:41', 'bryan.useche@sapi.gob.ve'),
(122, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:55:41', 'bryan.useche@sapi.gob.ve'),
(123, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:56:08', 'bryan.useche@sapi.gob.ve'),
(124, '::1', 'addentrada', '{\"numfac\":\"25478\",\"fecfac\":\"2020-07-17\",\"provid\":\"4\",\"fecent\":\"2020-07-21\",\"entcoment\":\"Carga de prueba\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:56:19', 'bryan.useche@sapi.gob.ve'),
(125, '::1', 'buscarxcodbar', '{\"data\":\"5010000091\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:56:33', 'bryan.useche@sapi.gob.ve'),
(126, '::1', 'addproduct', '{\"codbar\":\"5010000091\",\"prodmar\":\"Cuadernos Orinoco\",\"prodmodel\":\"Cuaderno de Una Linea 208 Paginas\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:56:44', 'bryan.useche@sapi.gob.ve'),
(127, '::1', 'adddetalle', '{\"codbar\":\"5010000091\",\"regent\":\"1\",\"numunid\":\"250\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:56:57', 'bryan.useche@sapi.gob.ve'),
(128, '::1', 'adddetalle', '{\"codbar\":\"5010000091\",\"regent\":\"1\",\"numunid\":\"250\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:57:27', 'bryan.useche@sapi.gob.ve'),
(129, '::1', 'adddetalle', '{\"codbar\":\"5010000091\",\"regent\":\"1\",\"numunid\":\"250\",\"costuni\":656478.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:58:56', 'bryan.useche@sapi.gob.ve'),
(130, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 14:58:57', 'bryan.useche@sapi.gob.ve'),
(131, '::1', 'entradas', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:00:14', 'bryan.useche@sapi.gob.ve'),
(132, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:00:37', 'bryan.useche@sapi.gob.ve'),
(133, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:03:50', 'bryan.useche@sapi.gob.ve'),
(134, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:07:17', 'bryan.useche@sapi.gob.ve'),
(135, '::1', 'buscarproveedor', '{\"rif\":\"J00000002\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:08:00', 'bryan.useche@sapi.gob.ve'),
(136, '::1', 'buscarproveedor', '{\"rif\":\"J0000020\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:08:00', 'bryan.useche@sapi.gob.ve'),
(137, '::1', 'buscarproveedor', '{\"rif\":\"J000202\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:08:01', 'bryan.useche@sapi.gob.ve'),
(138, '::1', 'buscarproveedor', '{\"rif\":\"J02020\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:08:01', 'bryan.useche@sapi.gob.ve'),
(139, '::1', 'buscarproveedor', '{\"rif\":\"J20200\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:08:02', 'bryan.useche@sapi.gob.ve'),
(140, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:08:19', 'bryan.useche@sapi.gob.ve'),
(141, '::1', 'buscarproveedor', '{\"rif\":\"J20200\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:08:37', 'bryan.useche@sapi.gob.ve'),
(142, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:08:54', 'bryan.useche@sapi.gob.ve'),
(143, '::1', 'buscarproveedor', '{\"rif\":\"J20200\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:09:14', 'bryan.useche@sapi.gob.ve'),
(144, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:09:46', 'bryan.useche@sapi.gob.ve'),
(145, '::1', 'buscarproveedor', '{\"rif\":\"J20200\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:10:02', 'bryan.useche@sapi.gob.ve'),
(146, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:10:35', 'bryan.useche@sapi.gob.ve'),
(147, '::1', 'buscarproveedor', '{\"rif\":\"J20200\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:10:53', 'bryan.useche@sapi.gob.ve'),
(148, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:13:32', 'bryan.useche@sapi.gob.ve'),
(149, '::1', 'buscarproveedor', '{\"rif\":\"J20200\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:13:50', 'bryan.useche@sapi.gob.ve'),
(150, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:15:12', 'bryan.useche@sapi.gob.ve'),
(151, '::1', 'buscarproveedor', '{\"rif\":\"J20200\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:15:54', 'bryan.useche@sapi.gob.ve'),
(152, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:16:36', 'bryan.useche@sapi.gob.ve'),
(153, '::1', 'addentrada', '{\"numfac\":\"54785\",\"fecfac\":\"2020-07-17\",\"provid\":\"\",\"fecent\":\"2020-07-21\",\"entcoment\":\"Carga de prueba\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:17:27', 'bryan.useche@sapi.gob.ve'),
(154, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:17:34', 'bryan.useche@sapi.gob.ve'),
(155, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:17:50', 'bryan.useche@sapi.gob.ve'),
(156, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:18:13', 'bryan.useche@sapi.gob.ve'),
(157, '::1', 'addentrada', '{\"numfac\":\"54785\",\"fecfac\":\"2020-07-17\",\"provid\":\"4\",\"fecent\":\"2020-07-21\",\"entcoment\":\"Carga de prueba\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:18:19', 'bryan.useche@sapi.gob.ve'),
(158, '::1', 'buscarxcodbar', '{\"data\":\"5010000091\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:18:34', 'bryan.useche@sapi.gob.ve'),
(159, '::1', 'addproduct', '{\"codbar\":\"5010000091\",\"prodmar\":\"Cuadernos Orinoco\",\"prodmodel\":\"Cuaderno de Una Linea 208 Paginas\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:18:42', 'bryan.useche@sapi.gob.ve'),
(160, '::1', 'adddetalle', '{\"codbar\":\"5010000091\",\"regent\":\"1\",\"numunid\":\"280\",\"costuni\":350000,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:18:56', 'bryan.useche@sapi.gob.ve'),
(161, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:18:57', 'bryan.useche@sapi.gob.ve'),
(162, '::1', 'buscarxcodbar', '{\"data\":\"7591447360022\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:20:26', 'bryan.useche@sapi.gob.ve'),
(163, '::1', 'addproduct', '{\"codbar\":\"7591447360022\",\"prodmar\":\"Mayka\",\"prodmodel\":\"6 Colores Plasticos Maykolor\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:20:58', 'bryan.useche@sapi.gob.ve'),
(164, '::1', 'adddetalle', '{\"codbar\":\"7591447360022\",\"regent\":\"1\",\"numunid\":\"500\",\"costuni\":112358.35,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:21:20', 'bryan.useche@sapi.gob.ve'),
(165, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:21:21', 'bryan.useche@sapi.gob.ve'),
(166, '::1', 'buscarxcodbar', '{\"data\":\"7750082039209\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:21:54', 'bryan.useche@sapi.gob.ve'),
(167, '::1', 'addproduct', '{\"codbar\":\"7750082039209\",\"prodmar\":\"Artesco\",\"prodmodel\":\"Lapices 2HB Grafiplus\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:22:23', 'bryan.useche@sapi.gob.ve'),
(168, '::1', 'adddetalle', '{\"codbar\":\"7750082039209\",\"regent\":\"1\",\"numunid\":\"2400\",\"costuni\":56825.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:23:11', 'bryan.useche@sapi.gob.ve'),
(169, '::1', 'detalles', '{\"numregent\":\"1\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:23:11', 'bryan.useche@sapi.gob.ve'),
(170, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:25:06', 'bryan.useche@sapi.gob.ve'),
(171, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:27:05', 'bryan.useche@sapi.gob.ve'),
(172, '::1', 'addentrada', '{\"numfac\":\"625478\",\"fecfac\":\"2020-07-13\",\"provid\":\"4\",\"fecent\":\"2020-07-21\",\"entcoment\":\"Carga de prueba 2\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:27:13', 'bryan.useche@sapi.gob.ve'),
(173, '::1', 'buscarxcodbar', '{\"data\":\"9788466616843\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:29:55', 'bryan.useche@sapi.gob.ve'),
(174, '::1', 'addproduct', '{\"codbar\":\"9788466616843\",\"prodmar\":\"Ediciones B\",\"prodmodel\":\"Libro Michael Moore ?Qu? han hecho con mi pa?s?\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:30:32', 'bryan.useche@sapi.gob.ve'),
(175, '::1', 'adddetalle', '{\"codbar\":\"9788466616843\",\"regent\":\"2\",\"numunid\":\"300\",\"costuni\":115325.62,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:30:50', 'bryan.useche@sapi.gob.ve'),
(176, '::1', 'detalles', '{\"numregent\":\"2\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:30:51', 'bryan.useche@sapi.gob.ve'),
(177, '::1', 'buscarxcodbar', '{\"data\":\"9789803810887\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:31:22', 'bryan.useche@sapi.gob.ve'),
(178, '::1', 'addproduct', '{\"codbar\":\"9789803810887\",\"prodmar\":\"Magistral\",\"prodmodel\":\"Nuevo Diccionario Magistral Espa?ol Ingles\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:32:22', 'bryan.useche@sapi.gob.ve'),
(179, '::1', 'adddetalle', '{\"codbar\":\"9789803810887\",\"regent\":\"2\",\"numunid\":\"200\",\"costuni\":112358.35,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:32:32', 'bryan.useche@sapi.gob.ve'),
(180, '::1', 'detalles', '{\"numregent\":\"2\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:32:33', 'bryan.useche@sapi.gob.ve'),
(181, '::1', 'buscarxcodbar', '{\"data\":\"9788497595308\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:34:26', 'bryan.useche@sapi.gob.ve'),
(182, '::1', 'addproduct', '{\"codbar\":\"9788497595308\",\"prodmar\":\"DeBolsillo\",\"prodmodel\":\"Libro La Isla de las Tormentas, Ken Follet\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:34:45', 'bryan.useche@sapi.gob.ve'),
(183, '::1', 'adddetalle', '{\"codbar\":\"9788497595308\",\"regent\":\"2\",\"numunid\":\"300\",\"costuni\":56825.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:34:55', 'bryan.useche@sapi.gob.ve'),
(184, '::1', 'detalles', '{\"numregent\":\"2\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:34:56', 'bryan.useche@sapi.gob.ve'),
(185, '::1', 'entradas', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:35:14', 'bryan.useche@sapi.gob.ve'),
(186, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:38:53', 'bryan.useche@sapi.gob.ve'),
(187, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:39:44', 'bryan.useche@sapi.gob.ve');
INSERT INTO `sta_log` (`id`, `access_ip`, `url_request`, `data_request`, `user_agent`, `access_date`, `user_access`) VALUES
(188, '::1', 'addentrada', '{\"numfac\":\"65477\",\"fecfac\":\"2020-06-03\",\"provid\":\"4\",\"fecent\":\"2020-07-21\",\"entcoment\":\"test 3\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:39:52', 'bryan.useche@sapi.gob.ve'),
(189, '::1', 'buscarxcodbar', '{\"data\":\"9788401422812\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:40:10', 'bryan.useche@sapi.gob.ve'),
(190, '::1', 'addproduct', '{\"codbar\":\"9788401422812\",\"prodmar\":\"Plaza Janes\",\"prodmodel\":\"Edgar Allan Poe Historias Extraordinarias\",\"modform\":1}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:40:43', 'bryan.useche@sapi.gob.ve'),
(191, '::1', 'adddetalle', '{\"codbar\":\"9788401422812\",\"regent\":\"3\",\"numunid\":\"300\",\"costuni\":112358.35,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:40:54', 'bryan.useche@sapi.gob.ve'),
(192, '::1', 'detalles', '{\"numregent\":\"3\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:40:55', 'bryan.useche@sapi.gob.ve'),
(193, '::1', 'buscarxcodbar', '{\"data\":\"9788497595308\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:42:43', 'bryan.useche@sapi.gob.ve'),
(194, '::1', 'adddetalle', '{\"codbar\":\"9788497595308\",\"regent\":\"3\",\"numunid\":\"200\",\"costuni\":56825.25,\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:42:54', 'bryan.useche@sapi.gob.ve'),
(195, '::1', 'regentrada', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:43:34', 'bryan.useche@sapi.gob.ve'),
(196, '::1', 'buscarproveedor', '{\"rif\":\"J406275433\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:43:53', 'bryan.useche@sapi.gob.ve'),
(197, '::1', 'addentrada', '{\"numfac\":\"32547\",\"fecfac\":\"2020-07-16\",\"provid\":\"4\",\"fecent\":\"2020-07-21\",\"entcoment\":\"tst 3\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:43:59', 'bryan.useche@sapi.gob.ve'),
(198, '::1', 'buscarxcodbar', '{\"data\":\"9788497595308\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:44:09', 'bryan.useche@sapi.gob.ve'),
(199, '::1', 'adddetalle', '{\"codbar\":\"9788497595308\",\"regent\":\"4\",\"numunid\":\"200\",\"costuni\":\"112358.35\",\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:44:19', 'bryan.useche@sapi.gob.ve'),
(200, '::1', 'adddetalle', '{\"codbar\":\"9788497595308\",\"regent\":\"4\",\"numunid\":\"200\",\"costuni\":\"112358.35\",\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:44:38', 'bryan.useche@sapi.gob.ve'),
(201, '::1', 'detalles', '{\"numregent\":\"4\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:44:38', 'bryan.useche@sapi.gob.ve'),
(202, '::1', 'buscarxcodbar', '{\"data\":\"9789803810887\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:46:11', 'bryan.useche@sapi.gob.ve'),
(203, '::1', 'adddetalle', '{\"codbar\":\"9789803810887\",\"regent\":\"4\",\"numunid\":\"320\",\"costuni\":\"656478.25\",\"prodpresent\":\"Caja\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:46:22', 'bryan.useche@sapi.gob.ve'),
(204, '::1', 'detalles', '{\"numregent\":\"4\"}', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:46:22', 'bryan.useche@sapi.gob.ve'),
(205, '::1', 'entradas', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:47:16', 'bryan.useche@sapi.gob.ve'),
(206, '::1', 'existencias', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:50:22', 'bryan.useche@sapi.gob.ve'),
(207, '::1', 'existencias', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:51:10', 'bryan.useche@sapi.gob.ve'),
(208, '::1', 'existencias', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:51:52', 'bryan.useche@sapi.gob.ve'),
(209, '::1', 'acttabla', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:51:59', 'bryan.useche@sapi.gob.ve'),
(210, '::1', 'existencias', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:52:35', 'bryan.useche@sapi.gob.ve'),
(211, '::1', 'acttabla', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:52:42', 'bryan.useche@sapi.gob.ve'),
(212, '::1', 'existencias', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:54:02', 'bryan.useche@sapi.gob.ve'),
(213, '::1', 'acttabla', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:54:08', 'bryan.useche@sapi.gob.ve'),
(214, '::1', 'existencias', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:55:10', 'bryan.useche@sapi.gob.ve'),
(215, '::1', 'acttabla', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:55:17', 'bryan.useche@sapi.gob.ve'),
(216, '::1', 'existencias', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:55:36', 'bryan.useche@sapi.gob.ve'),
(217, '::1', 'acttabla', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:55:43', 'bryan.useche@sapi.gob.ve'),
(218, '::1', 'existencias', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:57:00', 'bryan.useche@sapi.gob.ve'),
(219, '::1', 'acttabla', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:57:07', 'bryan.useche@sapi.gob.ve'),
(220, '::1', 'logout', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:57:49', 'bryan.useche@sapi.gob.ve'),
(221, '::1', '/', NULL, 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/79.0.3945.79 Chrome/79.0.3945.79 Safari/537.36', '2020-07-21 15:57:50', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_ordenes`
--

CREATE TABLE `sta_ordenes` (
  `numorden` int(11) NOT NULL,
  `fecaprob` date NOT NULL,
  `statusid` int(11) NOT NULL,
  `usuaprob` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci COMMENT='tabla de ordenes aprobadas';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_preordenes`
--

CREATE TABLE `sta_preordenes` (
  `numorden` int(11) NOT NULL,
  `fecsol` date NOT NULL,
  `ordstatus` int(11) NOT NULL,
  `usureg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci COMMENT='Tabla de preordenes del usuario';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_productos`
--

CREATE TABLE `sta_productos` (
  `codbar` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `prodmar` varchar(128) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `prodmodel` varchar(128) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `sta_productos`
--

INSERT INTO `sta_productos` (`codbar`, `prodmar`, `prodmodel`) VALUES
('5010000091', 'Cuadernos Orinoco', 'Cuaderno de Una Linea 208 Paginas'),
('7591447360022', 'Mayka', '6 Colores Plasticos Maykolor'),
('7750082039209', 'Artesco', 'Lapices 2HB Grafiplus'),
('9788401422812', 'Plaza Janes', 'Edgar Allan Poe Historias Extraordinarias'),
('9788466616843', 'Ediciones B', 'Libro Michael Moore ¿Qué han hecho con mi país?'),
('9788497595308', 'DeBolsillo', 'Libro La Isla de las Tormentas, Ken Follet'),
('9789803810887', 'Magistral', 'Nuevo Diccionario Magistral Español Ingles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_proveedores`
--

CREATE TABLE `sta_proveedores` (
  `idprov` int(11) NOT NULL,
  `numrif` varchar(32) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `nomprov` varchar(32) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `direccprov` varchar(512) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `telef1` varchar(15) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `telef2` varchar(15) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `email` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `sta_proveedores`
--

INSERT INTO `sta_proveedores` (`idprov`, `numrif`, `nomprov`, `direccprov`, `telef1`, `telef2`, `email`) VALUES
(1, 'V', '', '', '0', '0', ''),
(2, 'V208258409', 'Universal Music', 'Av ningun lugar, edificio abandonado , piso 5, oficina 5a', '02120602517', '02127848207', 'alguien@universal.com.ve'),
(3, 'J23456789', 'Inversiones sospechosas S.A', 'Av ningun lugar, edificio abandonado , piso 5, oficina 5a', '02120602517', '02127848207', 'alguien@universal.com.ve'),
(4, 'J406275433', 'Cuadernos Orinoco', 'Caracas', '021283068668', '02128306870', 'alguien@microsoft.com'),
(5, 'J000202001', 'Farmatodo C.A', 'Av Los Guayabitos, CC Expreso Baruta, Nivel 5, Oficina Unica. Urb La Trinidad, Sector Piedra azul, Caracas', '0800327628636', '02128306870', 'alguien@microsoft.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_roles`
--

CREATE TABLE `sta_roles` (
  `idrol` int(11) NOT NULL,
  `rolnom` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'Nombre del rol'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `sta_roles`
--

INSERT INTO `sta_roles` (`idrol`, `rolnom`) VALUES
(1, 'Soporte'),
(2, 'Usuario'),
(3, 'Administrador'),
(4, 'Almacenista');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_status`
--

CREATE TABLE `sta_status` (
  `statusid` int(11) NOT NULL COMMENT 'ID del estatus de la operacion',
  `statusnom` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL COMMENT 'Descripcion del estatus'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `sta_status`
--

INSERT INTO `sta_status` (`statusid`, `statusnom`) VALUES
(1, 'En Tramite'),
(2, 'Aprobado'),
(3, 'Despachado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sta_usuarios`
--

CREATE TABLE `sta_usuarios` (
  `userid` int(11) NOT NULL,
  `usupnom` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `ususnom` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `usupape` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `ususape` varchar(48) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `usuemail` varchar(128) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `usupass` varchar(148) COLLATE utf8mb4_spanish2_ci NOT NULL,
  `deptid` int(11) NOT NULL,
  `idrol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish2_ci;

--
-- Volcado de datos para la tabla `sta_usuarios`
--

INSERT INTO `sta_usuarios` (`userid`, `usupnom`, `ususnom`, `usupape`, `ususape`, `usuemail`, `usupass`, `deptid`, `idrol`) VALUES
(2, 'Bryan', 'Ramon', 'Useche', 'Tapias', 'bryan.useche@sapi.gob.ve', '445802dae990291723acd859d0a98e93', 1, 1),
(3, 'Usuario', 'De ', 'Prueba ', 'uno', 'usertest1@sapi.gob.ve', '445802dae990291723acd859d0a98e93', 1, 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sta_almacen_entradas`
--
ALTER TABLE `sta_almacen_entradas`
  ADD PRIMARY KEY (`numregent`),
  ADD KEY `entradas_proveedor` (`provid`),
  ADD KEY `usuario_registrador` (`usuregent`);

--
-- Indices de la tabla `sta_almacen_salidas`
--
ALTER TABLE `sta_almacen_salidas`
  ADD PRIMARY KEY (`salidaid`),
  ADD UNIQUE KEY `numorden` (`numorden`),
  ADD KEY `despacho_destino` (`depdest`),
  ADD KEY `despacho_estatus` (`statusid`);

--
-- Indices de la tabla `sta_departamentos`
--
ALTER TABLE `sta_departamentos`
  ADD PRIMARY KEY (`deptid`);

--
-- Indices de la tabla `sta_dep_dir`
--
ALTER TABLE `sta_dep_dir`
  ADD PRIMARY KEY (`unionid`),
  ADD KEY `departamentos` (`depid`),
  ADD KEY `direcciones` (`dirid`);

--
-- Indices de la tabla `sta_detalles_ordenes`
--
ALTER TABLE `sta_detalles_ordenes`
  ADD PRIMARY KEY (`itemid`),
  ADD KEY `detalle_numorden` (`numorden`),
  ADD KEY `detalle_producto_orden` (`codbar`);

--
-- Indices de la tabla `sta_detalles_preordenes`
--
ALTER TABLE `sta_detalles_preordenes`
  ADD PRIMARY KEY (`detalleid`),
  ADD KEY `detalle_preorden` (`numorden`),
  ADD KEY `detalle_producto` (`codbar`);

--
-- Indices de la tabla `sta_direcciones`
--
ALTER TABLE `sta_direcciones`
  ADD PRIMARY KEY (`dirid`);

--
-- Indices de la tabla `sta_entradas_detalles`
--
ALTER TABLE `sta_entradas_detalles`
  ADD PRIMARY KEY (`itemid`),
  ADD KEY `num_entrada` (`regent`),
  ADD KEY `item_codigo_barras` (`codbar`);

--
-- Indices de la tabla `sta_existencias`
--
ALTER TABLE `sta_existencias`
  ADD PRIMARY KEY (`itemid`),
  ADD KEY `existencia_producto` (`codbar`);

--
-- Indices de la tabla `sta_ordenes`
--
ALTER TABLE `sta_ordenes`
  ADD UNIQUE KEY `numorden` (`numorden`),
  ADD KEY `orden_estatus` (`statusid`);

--
-- Indices de la tabla `sta_preordenes`
--
ALTER TABLE `sta_preordenes`
  ADD PRIMARY KEY (`numorden`),
  ADD KEY `preorden_status` (`ordstatus`),
  ADD KEY `preorden_usuarios` (`usureg`);

--
-- Indices de la tabla `sta_productos`
--
ALTER TABLE `sta_productos`
  ADD PRIMARY KEY (`codbar`);

--
-- Indices de la tabla `sta_proveedores`
--
ALTER TABLE `sta_proveedores`
  ADD PRIMARY KEY (`idprov`);

--
-- Indices de la tabla `sta_roles`
--
ALTER TABLE `sta_roles`
  ADD PRIMARY KEY (`idrol`);

--
-- Indices de la tabla `sta_status`
--
ALTER TABLE `sta_status`
  ADD PRIMARY KEY (`statusid`);

--
-- Indices de la tabla `sta_usuarios`
--
ALTER TABLE `sta_usuarios`
  ADD PRIMARY KEY (`userid`),
  ADD KEY `roles` (`idrol`),
  ADD KEY `usuario_departamento` (`deptid`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `sta_almacen_entradas`
--
ALTER TABLE `sta_almacen_entradas`
  MODIFY `numregent` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `sta_almacen_salidas`
--
ALTER TABLE `sta_almacen_salidas`
  MODIFY `salidaid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sta_departamentos`
--
ALTER TABLE `sta_departamentos`
  MODIFY `deptid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sta_dep_dir`
--
ALTER TABLE `sta_dep_dir`
  MODIFY `unionid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sta_detalles_ordenes`
--
ALTER TABLE `sta_detalles_ordenes`
  MODIFY `itemid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sta_detalles_preordenes`
--
ALTER TABLE `sta_detalles_preordenes`
  MODIFY `detalleid` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sta_direcciones`
--
ALTER TABLE `sta_direcciones`
  MODIFY `dirid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sta_entradas_detalles`
--
ALTER TABLE `sta_entradas_detalles`
  MODIFY `itemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `sta_existencias`
--
ALTER TABLE `sta_existencias`
  MODIFY `itemid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `sta_log`
--
ALTER TABLE `sta_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sta_preordenes`
--
ALTER TABLE `sta_preordenes`
  MODIFY `numorden` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `sta_proveedores`
--
ALTER TABLE `sta_proveedores`
  MODIFY `idprov` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `sta_roles`
--
ALTER TABLE `sta_roles`
  MODIFY `idrol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `sta_status`
--
ALTER TABLE `sta_status`
  MODIFY `statusid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID del estatus de la operacion', AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `sta_usuarios`
--
ALTER TABLE `sta_usuarios`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `sta_almacen_entradas`
--
ALTER TABLE `sta_almacen_entradas`
  ADD CONSTRAINT `entradas_proveedor` FOREIGN KEY (`provid`) REFERENCES `sta_proveedores` (`idprov`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_registrador` FOREIGN KEY (`usuregent`) REFERENCES `sta_usuarios` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sta_almacen_salidas`
--
ALTER TABLE `sta_almacen_salidas`
  ADD CONSTRAINT `despacho_destino` FOREIGN KEY (`depdest`) REFERENCES `sta_departamentos` (`deptid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `despacho_estatus` FOREIGN KEY (`statusid`) REFERENCES `sta_status` (`statusid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `despacho_ordensalida` FOREIGN KEY (`numorden`) REFERENCES `sta_ordenes` (`numorden`);

--
-- Filtros para la tabla `sta_dep_dir`
--
ALTER TABLE `sta_dep_dir`
  ADD CONSTRAINT `departamentos` FOREIGN KEY (`depid`) REFERENCES `sta_departamentos` (`deptid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `direcciones` FOREIGN KEY (`dirid`) REFERENCES `sta_direcciones` (`dirid`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sta_detalles_ordenes`
--
ALTER TABLE `sta_detalles_ordenes`
  ADD CONSTRAINT `detalle_numorden` FOREIGN KEY (`numorden`) REFERENCES `sta_ordenes` (`numorden`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detalle_producto_orden` FOREIGN KEY (`codbar`) REFERENCES `sta_productos` (`codbar`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sta_detalles_preordenes`
--
ALTER TABLE `sta_detalles_preordenes`
  ADD CONSTRAINT `detalle_preorden` FOREIGN KEY (`numorden`) REFERENCES `sta_preordenes` (`numorden`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detalle_producto` FOREIGN KEY (`codbar`) REFERENCES `sta_productos` (`codbar`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sta_entradas_detalles`
--
ALTER TABLE `sta_entradas_detalles`
  ADD CONSTRAINT `item_codigo_barras` FOREIGN KEY (`codbar`) REFERENCES `sta_productos` (`codbar`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `num_entrada` FOREIGN KEY (`regent`) REFERENCES `sta_almacen_entradas` (`numregent`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sta_existencias`
--
ALTER TABLE `sta_existencias`
  ADD CONSTRAINT `existencia_producto` FOREIGN KEY (`codbar`) REFERENCES `sta_productos` (`codbar`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `sta_ordenes`
--
ALTER TABLE `sta_ordenes`
  ADD CONSTRAINT `orden_estatus` FOREIGN KEY (`statusid`) REFERENCES `sta_status` (`statusid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `preorden_orden` FOREIGN KEY (`numorden`) REFERENCES `sta_preordenes` (`numorden`);

--
-- Filtros para la tabla `sta_preordenes`
--
ALTER TABLE `sta_preordenes`
  ADD CONSTRAINT `preorden_status` FOREIGN KEY (`ordstatus`) REFERENCES `sta_status` (`statusid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `preorden_usuarios` FOREIGN KEY (`usureg`) REFERENCES `sta_usuarios` (`userid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `sta_usuarios`
--
ALTER TABLE `sta_usuarios`
  ADD CONSTRAINT `roles` FOREIGN KEY (`idrol`) REFERENCES `sta_roles` (`idrol`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_departamento` FOREIGN KEY (`deptid`) REFERENCES `sta_departamentos` (`deptid`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
