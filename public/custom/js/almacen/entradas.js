$("#detfac").hide();

/*Funcion que obtiene la tabla de detalles*/
function actualizarTablaDetalles(numregent){
	$.ajax({
		url:'/detalles',
		method:'POST',
		data: {data:btoa(JSON.stringify({numregent: numregent}))},
		dataType:'JSON',
		success: function(response){
			$("#detallesFactura").html(response.data);
		},
		error:function(request){
			Swal.fire('Error!', request.responseJSON.message, 'error');
		}
	});
}

/*FUncion que despliega un calendario*/
function calendario(selector){
    $(selector).daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        maxDate: fecha(),
        locale: {
            format: 'DD/MM/YYYY',
            daysOfWeek: [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        monthNames: [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ]
        }
    });
}

/*Funcion que realiza la fecha de hoy*/
function fecha(){
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1;
    var yy = hoy.getFullYear();
    var fecha = '';
    if(dd<10){
        dd = '0'+dd;
    }
    else if(mm<10){
        mm = '0'+mm;
    }
    fecha = dd+"/"+mm+"/"+yy;
    return fecha;
}

function invertirFecha(fecha){
	let fectmp = fecha.split('/');
	let fechadb = `${fectmp[2]}-${fectmp[1]}-${fectmp[0]}`;
	return fechadb;
}

$("#fecfac").on('focus', function(){
	calendario(this);
});
/*Evento para buscar un proveedor en la BD*/
$("#rifprov").on('keyup',function(e){
	e.preventDefault();
	let rif = $("#tipoprov").val()+$(this).val();
	if(rif.length == 10){
		$.ajax({
			url:'/buscarproveedor',
			method:'POST',
			data:{"data": btoa(JSON.stringify({"rif":rif}))},
			success:function(response){
				$("#idprov").val(response.data.idprov);
				$("#nomprov").val(response.data.nomprov);
			},
			error:function(request){
				$("#frmnewnumrif").val(rif);
				$("#add-proveedor").modal('show');
			}
		});
	}
});

/*Evento cuando un proveedor no este*/
$("#newProvider").on('submit', function(e){
	e.preventDefault();
	var form = {
		numrif     : String($("#frmnewnumrif").val()),
		nomprov    : String($("#frmnewnomprov").val()),
		direccprov : String($("#frmnewdireccprov").val()),
		telef1     : String($("#frmnewtelef1").val()),
		telef2     : String($("#frmnewtelef2").val()),
		email      : String($("#frmnewcontemail").val())
	};
	$.ajax({
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		url:'/nuevoproveedor',
		method:'POST',
		data: {data: btoa(JSON.stringify(form))},
		dataType:'JSON',
		beforeSend:function(){
			$("button[type=submit").attr('disabled', 'true');
		},
		success:function(response){
			$.ajax({
				url:'/buscarproveedor',
				method:'POST',
				data:{"data": btoa(JSON.stringify({"rif":form.numrif}))},
				success:function(response){
					$("#idprov").val(response.data.idprov);
					$("#nomprov").val(response.data.nomprov);
					$("#newProvider")[0].reset();
				},
			});
			$("#tipoprov").val(form.numrif.slice(0,0));
			$("#rifprov").val(form.numrif.slice(1,9));
			$("#add-proveedor").modal('hide');
		},
		error:function(request){
			Swal.fire('Error!', request.responseJSON.message, 'error');

		}
	});
	$("button[type=submit").removeAttr('disabled');
});

/*Evento que registra los datos de entrada del almacen*/
$("#datos-entrada").on('submit', function(e){
	e.preventDefault();
	let form = {
		numfac   : $("#numfac").val(),
		fecfac   : invertirFecha($("#fecfac").val()),
		provid   : $("#idprov").val(),
		fecent   : invertirFecha($("#fecent").val()),
		entcoment: $("#entcoment").val()
	};
	$.ajax({
		url:'/addentrada',
		method:'POST',
		data:{data:btoa(JSON.stringify(form))},
		dataType:'JSON',
		beforeSend:function(){
			$("button[type=submit").attr('disabled', 'true');
		},
		success:function(response){
			$("#numregent").val(response.num_op);
			$("button[type=submit").removeAttr('disabled');
			$("#datos-entrada")[0].reset();
			$("#facdata").hide();
			$("#detfac").show();
		},
		error:function(request){
			Swal.fire('Error!', request.responseJSON.message, 'error');			
		}
	});
});

/*Evento de busqueda de un nuevo producto*/
$(".buscar-cod-bar").on('click', function(e){
	let codbar = $("#codbar").val();
	$.ajax({
		url:'/buscarxcodbar',
		method:'POST',
		data:{data : btoa(JSON.stringify({data:codbar}))},
		dataType:'JSON',
		success: function(response){
			$("#prodmar").val(response.data.prodmar);
			$("#prodmodel").val(response.data.prodmodel);
		},
		error: function(request){
			$("#frmnewcodbar").val(codbar);
			$("#add-producto").modal('show');
		}
	});
});
/*Evento de registro de un nuevo producto*/
$("#newProduct").on('submit', function(e){
	e.preventDefault();
	let form = {
		codbar : $("#frmnewcodbar").val(),
		prodmar: $("#frmnewprodmar").val(),
		prodmodel: $("#frmnewprodmodel").val(),
		modform: 1
	}
	$.ajax({
		url:'/addproduct',
		method:'POST',
		data: {data: btoa(JSON.stringify(form))},
		dataType:'JSON',
		beforeSend:function(){
			$("button[type=submit]").attr('disabled', 'true');
		},
		success: function(response){
			$("#prodmar").val(form.prodmar);
			$("#prodmodel").val(form.prodmodel);
			$("#add-producto").modal('hide');
			$("#newProduct")[0].reset();
		},
		error: function(request){
			Swal.fire('Error!', request.responseJSON.message, 'error');
		}
	});
	$("button[type=submit]").removeAttr('disabled');
});

/*Evento de registro de un detalle de la factura*/
$("#adddetalle").on('submit', function(e){
	e.preventDefault();
	let form = {
		codbar     : $("#codbar").val(),
		regent     : $("#numregent").val(),
		numunid    : $("#numuni").val(),
		costuni    : $("#costuni").val(),
		prodpresent: $("#prodpresent").val(),
	}
	$.ajax({
		url:'/adddetalle',
		method:'POST',
		data:{data:btoa(JSON.stringify(form))},
		dataType:'JSON',
		beforeSend:function(){
			$("button[type=submit]").attr('disabled', 'true');
		},
		success: function(response){
			$("#adddetalle")[0].reset();
			$("#add-detalle-factura").modal('hide');
			//Refrescamos la tabla
			actualizarTablaDetalles(form.regent);
		},
		error: function(request){
			Swal.fire('Error!', request.responseJSON.message, 'error');
		}
	});
	$("button[type=submit]").removeAttr('disabled');
});