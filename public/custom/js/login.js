/*Autenticacion de usuario*/
$(document).on('submit', '#login', function(e){
	e.preventDefault();
	let datos = {
		usuemail : $("#usuemail").val(),
		usupass  : $("#usupass").val()
	}
	$.ajax({
		url:'/signin',
		method:'POST',
		data:{data: btoa(JSON.stringify(datos))},
		dataType:'JSON',
		beforeSend:function(){
			$("button[type=submit]").addClass('disabled');
			Swal.fire({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000,
				type: 'info',
        		title: 'Verficando informacion'
			});
		},
		success:function(response){
			Swal.fire({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000,
				type: 'success',
        		title: response.message,
			});
			window.location = '/inicio';
			$("button[type=submit]").removeClass('disabled');
		},
		error: function(request){
			$("button[type=submit]").removeClass('disabled');
			Swal.fire({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 3000,
				type: 'error',
        		title: request.responseJSON.message,
			});
		}
	});
});

/*Verficacion de datos en el form*/
$(document).on('change', '#usuemail',function(e){
	let texto = $("#usuemail").val();
	if(texto.match(/\w*.\w*\@sapi.gob.ve/) == null){
		$("button[type=submit]").removeClass('is-valid');
		$("#usuemail").addClass('is-invalid');
		$("button[type=submit]").attr('disabled', 'true');
	}
	else if(texto.lenght < 5){
		$("#usuemail").removeClass('is-invalid');
		$("#usuemail").removeClass('is-valid');
		$("button[type=submit]").removeAttr('disabled');
	}
	else{
		$("#usuemail").removeClass('is-invalid');
		$("#usuemail").addClass('is-valid');
		$("button[type=submit]").removeAttr('disabled');	
	}
});