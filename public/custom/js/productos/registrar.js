$(document).on('submit', '#nuevoproducto', function(e){
	e.preventDefault();
	var form = {
		'modform'  : $("#modform").val(),
		'codbar'   : $("#codbar").val(),
		'prodmar'  : $("#prodmar").val(),
		'prodmodel': $("#prodmodel").val()
	};
	$.ajax({
		url:'/addproduct',
		method:'POST',
		data: {'datos': btoa(JSON.stringify(form))},
		dataType:'JSON',
		beforeSend:function(){
			$("button[type=submit").attr('disabled', 'true');
		},
		success:function(response){
			window.location = '/consultaproducto'
		},
		error:function(request){
			Swal.fire('Error!', request.responseJSON.message, 'error');

		}
	});
	$("button[type=submit").removeAttr('disabled');
});