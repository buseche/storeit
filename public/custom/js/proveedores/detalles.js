$(document).on('click', '.detalles', function(){
	let id = $(this).attr('id');
	$.ajax({
		url:'/detalleproveedor',
		method:'POST',
		dataType:'JSON',
		data: {data: id},
		success:function(response){
			$("#detdirecc").val(response.data.direccprov);
			$("#detemail").val(response.data.contemail);
			$("#detalleProveedor").modal('show');
		},
		error:function(request){
			Swal.fire('error', response.message, 'error');
		}
	});
});

$(document).on('click', '.editar', function(){
	let id = $(this).attr('id');
	$.ajax({
		url:'/detalleproveedor',
		method:'POST',
		dataType:'JSON',
		data: {data: id},
		success:function(response){
			$("#direccprov").val(response.data.direccprov);
			$("#contemail").val(response.data.contemail);
			$("#editarProveedor").modal('show');
		},
		error:function(request){
			Swal.fire('error', response.message, 'error');
		}
	});
});


/*Para enviar el formulario de edicion*/
$(document).on('submit', '#editProvider', function(e){
	e.preventDefault();
	var form = {
		numrif     : String($("#tipoper").val()+$("#numrif").val()),
		nomprov    : String($("#nomprov").val()),
		direccprov : String($("#direccprov").val()),
		telef1     : String($("#telef1").val()),
		telef2     : String($("#telef2").val()),
		email      : String($("#contemail").val())
	};
	$.ajax({
		contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
		url:'/editarproveedor',
		method:'POST',
		data: {data: btoa(JSON.stringify(form))},
		dataType:'JSON',
		beforeSend:function(){
			$("button[type=submit").attr('disabled', 'true');
		},
		success:function(response){
			$("#editarProveedor").modal('hide');
		},
		error:function(request){
			Swal.fire('Error!', request.responseJSON.message, 'error');

		}
	});
	$("button[type=submit").removeAttr('disabled');
});