/*Funcion que elimina items de un arreglo*/
Array.prototype.removeItem = function (a) {
 for (var i = 0; i < this.length; i++) {
  if (this[i] == a) {
   for (var i2 = i; i2 < this.length - 1; i2++) {
    this[i2] = this[i2 + 1];
   }
   this.length = this.length - 1;
   return;
  }
 }
};

/*Funcion que carga los usuarios registrados en el sistema*/
function load_users(){
	$.ajax({
		url:'/usuarios',
		method:"GET",
		dataType:'JSON',
		success:function(response){
			let table = '';
			switch(response.status){
				case 200:
					/*Ponemos los select donde corresponda*/
					for(let i = 0; i<response.data.length; i++){
						let row = response.data[i];
						table = table + `<tr><td><input name="userval" class="user_record" type="checkbox" value="${row[0]}"></td><td>${row[1]}</td><td>${row[2]}</td><td>${row[3]}</td><td>${row[4]}</td><td>${row[5]}</td><td>${row[6]}</td><td><a href="/usuarios/${row[0]}">Editar</a></td></tr>`;
					}
					$("#users").html(table);
					break;
					case 404:
						/*Creamos la tabla para las direcciones*/
						table = `<tr><td colspan="7" class="text-center">Sin Registros</td></tr>`;
						$("#directions").html(table);
					break;
			}
		}
	});
}

/*Funcion que carga los datos de las direcciones segun sea el caso*/
function load_directions(){
	$.ajax({
		url:'/direcciones/',
		method:'GET',
		dataType:'JSON',
		success:function(response){
			let html = '<option>Seleccione una opcion</option>';
			let table = ''
			switch(response.status){
				case 200:
					/*Ponemos los select donde corresponda*/
					for(let i = 0; i<response.data.length; i++){
						let row = response.data[i];
						html = html + `<option value="${row[0]}">${row[1]}</option>`;
						table = table + `<tr><td><input name="dirval" class="dir_record" type="checkbox" value="${row[0]}"></td><td>${row[0]}</td><td>${row[1]}</td></tr>`;
					}
					$("#depdir").html(html);
					$("#usudir").html(html);
					$("#directions").html(table);
				break;
				case 404:
					/*Creamos la tabla para las direcciones*/
					select = '<option>No hay datos cargados</option>';
					table = '';
					table = table + `<tr><td colspan="3" class="text-center">Sin Registros</td></tr>`;
					$("#depdir").html(select);
					$("#usudir").html(select);
					$("#directions").html(table);
				break;
			}
		}
	});
}

/*Funcion que carga los datos de los roles segun sea el caso*/
function load_roles(){
	$.ajax({
		url:'/roles/',
		method:'GET',
		dataType:'JSON',
		success:function(response){
			let html = '<option>Seleccione una opcion</option>';
			let table = ''
			switch(response.status){
				case 200:
					/*Ponemos los select donde corresponda*/
					for(let i = 0; i<response.data.length; i++){
						let row = response.data[i];
						html = html + `<option value="${row[0]}">${row[1]}</option>`;
						table = table + `<tr><td><input name="rolval" class="rol_record" type="checkbox" value="${row[0]}"></td><td>${row[1]}</td></tr>`;
					}
					$("#usurol").html(html);
					$("#roles").html(table);
				break;
				case 404:
					/*Creamos la tabla para las direcciones*/
					select = '<option>No hay datos cargados</option>';
					table = '';
					table = table + `<tr><td colspan="3" class="text-center">Sin Registros</td></tr>`;
					$("#usurol").html(html);
					$("#roles").html(table);
				break;
			}
		}
	});
}



function load_data(){
	load_directions();
	load_departments();
	load_roles();
	load_users();
}
/*Funcion generica para crear registros*/
function create(datos, ruta, id_hidden, modal){
	$.ajax({
		url:ruta,
		method:'POST',
		dataType:'JSON',
		data:{data: datos},
		beforeSend:function(){
			$("button[type=submit]").addClass('disabled');
		},
		success:function(response){
			switch(response.status){
				case 200:
					Swal.fire('Exito!', 'Operacion realizada exitosamente', 'success');
					$(modal).modal('hide');
					$(id_hidden).val('');
					load_data();
					$("button[type=submit]").removeClass('disabled');
				break;
				default:
					Swal.fire("Error", 'Operacion no completada', 'error');
					$("button[type=submit]").removeClass('disabled');
				break;
			}
		},
		error:function(request){
			Swal.fire('Oops!', 'Ha ocurrido un error','error');
		}
	});
	return;
}

/*Funcion que carga los datos de los departamentos segun sea el caso*/
function load_departments(){
	$.ajax({
		url:'/departamentos',
		method:'GET',
		dataType:'JSON',
		success:function(response){
			let html = '';
			switch(response.status){
				case 200:
					/*Creamos la tabla para los departamentos*/
					html = '';
					for(let i = 0; i<response.data.length; i++){
						let row = response.data[i];
						html = html + `<tr><td><input name="deptval" class="dept_record" type="checkbox" value="${row[0]}"></td><td>${row[1]}</td><td>${row[2]}</td></tr>`;
					}
					$("#departaments").html(html);
				break;
				case 404:
					html = '';
					html = html + `<tr><td colspan="3" class="text-center">Sin Registros</td></tr>`;
					$("#departaments").html(html);
				break;
			}
		}
	})
}
/*Evento de carga de la pagina*/
$(document).ready(function(){
	/*Carga de datos*/
	load_data();
});
