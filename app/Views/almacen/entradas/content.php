<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Historico de entradas de productos</h3>

          <div class="card-tools">
            <a class="btn btn-sm btn-primary" href="/regentrada">Añadir</a>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-hover table-light">
            <thead>
              <tr>
                <td class="text-center">Nº Registro</td>
                <td class="text-center">Nº Factura</td>
                <td class="text-center">Proveedor</td>
                <td class="text-center">Fecha Factura</td>
                <td class="text-center">Fecha Operacion</td>
                <td class="text-center">Usuario</td>
                <td class="text-center">Comentario</td>
                <td class="text-center"></td>
              </tr>
            </thead>
            <tbody id="entradas">
              <?php echo $tbody;?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->