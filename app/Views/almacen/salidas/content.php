<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Blank Page</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Blank Page</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Registro de salidas</h3>

          <div class="card-tools">
            <a class="btn btn-primary btn-sm" href="/regsalida">Añadir</a>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-hover table-light">
            <thead>
              <tr>
                <td>Nº de Registro</td>
                <td>Fecha de Salida</td>
                <td>Nº de Orden</td>
                <td>Destino</td>
                <td>Procesado por</td>
                <td>Comentarios</td>
                <td></td>
              </tr>
            </thead>
            <tbody id="salidas">
              <?php echo $tbody;?>
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->