  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Listado de productos cargados</h3>

          <div class="card-tools">
            <a href="/regprod" class="btn btn-sm btn-primary">Añadir</a>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-hover table-light" id="listadoprod">
            <thead>
              <tr>
                <td>Código de barras</td>
                <td>Marca</td>
                <td>Descripcion</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <?php echo $tbody;?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.card -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->