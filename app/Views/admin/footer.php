
<!-- DataTables -->
<script src="<?php echo base_url();?>/theme/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>/theme/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- jquery-validation -->
<script src="<?php echo base_url();?>/theme/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="<?php echo base_url();?>/theme/plugins/jquery-validation/additional-methods.min.js"></script>
<!-- Custom Scripts-->
<script type="text/javascript" src="<?php echo base_url();?>/custom/js/admin.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/custom/js/admin/roles.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/custom/js/admin/direcciones.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/custom/js/admin/departamentos.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>/custom/js/admin/usuarios.js"></script>
</body>
</html>