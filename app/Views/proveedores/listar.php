<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Listado de proveedores registrados</h3>
          <div class="card-tools">
            <a href="/registrarproveedor" class="btn btn-primary btn-sm">Añadir</a>
          </div>
        </div>
        <div class="card-body">
          <table class="table table-hover table-light" id="listaproveedores">
            <thead>
              <tr>
                <td>RIF</td>
                <td>Nombre</td>
                <td>Telefono principal</td>
                <td>Email</td>
                <td></td>
              </tr>
            </thead>
            <tbody>
              <?php echo $tbody;?>
            </tbody>
          </table>
        </div>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->