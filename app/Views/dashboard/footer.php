	<script type="text/javascript">
		function tabPreordenes(){
			$.ajax({
				url:'/obtpreordenes',
				method:'POST',
				success: function(response){
					$("#preordenes").html(response.data);
				}
			});
		}

		function tabOrdenes(){
			$.ajax({
				url:'/obtordenes',
				method:'POST',
				success: function(response){
					$("#ordenes").html(response.data);
				}
			});
		}
		$(document).ready(function(){
			tabPreordenes();
			tabOrdenes();
		})
	</script>
</body>
</html>