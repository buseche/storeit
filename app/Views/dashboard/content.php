<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Tablero</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <!-- container-fluid -->
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>80</h3>

                <p>Requerimientos</p>
              </div>
              <div class="icon">
                <i class="fas fa-shopping-cart"></i>
              </div>
              
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>30<sup style="font-size: 20px">%</sup></h3>

                <p>Requerimientos aprobados</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>43</h3>

                <p>Requerimientos en espera</p>
              </div>
              <div class="icon">
                <i class="fas fa-user-plus"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small card -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>25</h3>

                <p>Requerimientos recibidos</p>
              </div>
              <div class="icon">
                <i class="fas fa-chart-pie"></i>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
        <div class="row">
          <section class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  Historico de requerimientos solicitados
                </h3>
              </div>
              <div class="card-body">
                <table class="table table-light table-hover table-bordered">
                  <thead>
                    <tr>
                      <td>Nº Orden</td>
                      <td>Fecha de Solicitud</td>
                      <td>Estatus</td>
                      <td>Detalles</td>
                    </tr>
                  </thead>
                  <tbody id="preordenes">
                  </tbody>
                </table>
              </div>
            </div>
          </section>
        </div>
        <div class="row">
          <section class="col-lg-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  Historico de solicitudes aprobadas y en tramites
                </h3>
              </div>
              <div class="card-body">
                <table class="table table-light table-hover">
                  <thead>
                    <tr>
                      <td>Nº Orden</td>
                      <td>Fecha de Solicitud</td>
                      <td>Estatus</td>
                      <td>Detalles</td>
                    </tr>
                  </thead>
                  <tbody id="ordenes">
                    
                  </tbody>
                </table>
              </div>
            </div>
          </section>
        </div>
        
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>