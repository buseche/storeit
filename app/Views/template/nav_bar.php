<?php $session = session();?>
<body class="hold-transition sidebar-mini layout-fixed sidebar-collapse">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/" class="nav-link">Inicio</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a id="logout" class="nav-link" href="#">
          <i class="fas fa-user"></i>
          Cerrar Sesion
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

    <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/inicio" class="brand-link">
      <img src="<?php echo base_url();?>/img/app.svg"
           alt="Sistema de almacen logo"
           class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Sistema de Almacen</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url();?>/theme/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="/perfil/<?php echo $session->get('userid');?>" class="d-block"><?php echo $session->get('usupnom').' '.$session->get('usupape');?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/inicio" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Tablero
              </p>
            </a>
          </li>
          <?php /*if($session->get('usurol') == 1){*/?>
          <!--Panel de control-->
          <li class="nav-item">
            <a href="/admin" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Panel de Control
              </p>
            </a>
          </li>
        <?php /*}
        elseif(($session->get('usurol') == 3 | $session->get('usurol') == 1)){
          */?>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-toolbox"></i>
              <p>
                Administrador
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <!--Solicitudes-->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="far fa-edit nav-icon"></i>
                  <p>
                    Solicitudes por aprobar
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Requisiciones</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Requerimientos</p>
                    </a>
                  </li>
                </ul>
              </li>
              <!--Existencias-->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="fas fa-search nav-icon"></i>
                  <p>
                    Consultas
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/existencias" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Existencias</p>
                    </a>
                  </li>
                </ul>
              </li>
              <!--Reportes-->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="fas fa-flag nav-icon"></i>
                  <p>
                    Reportes
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/reportes/5" class="nav-link">
                      <i class="far fa-calendar nav-icon"></i>
                      <p>Por Fecha</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/reportes/1" class="nav-link">
                      <i class="fas fa-apple-alt nav-icon"></i>
                      <p>Por Item o Articulo</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/reportes/4" class="nav-link">
                      <i class="fas fa-file-signature nav-icon"></i>
                      <p>Por Solicitud</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/reportes/3" class="nav-link">
                      <i class="fas fa-user nav-icon"></i>
                      <p>Por Usuario</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/reportes/6" class="nav-link">
                      <i class="far fa-chart-bar nav-icon"></i>
                      <p>Entradas y Salidas</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
        <?php /*} 
        elseif(($session->get('usurol') == 4 | $session->get('usurol') == 1)){
        */?>
          <!--Almacen -->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-store-alt"></i>
              <p>
                Almacen
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <!--Registros de entradas y salidas-->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Existencias
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/entradas" class="nav-link">
                      <i class="fas fa-arrow-left nav-icon"></i>
                      <p>Entradas</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/salidas" class="nav-link">
                      <i class="fas fa-arrow-right nav-icon"></i>
                      <p>Salidas</p>
                    </a>
                  </li>
                </ul>
              </li>
              <!--Productos -->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Productos
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/regprod" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Registrar Producto</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/consultaproducto" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Consultar Producto</p>
                    </a>
                  </li>
                </ul>
              </li>
              <!--Proveedores -->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Proveedores
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/newprovider" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Registrar Proveedor</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/consultaproveedor" class="nav-link">
                      <i class="far fa-dot-circle nav-icon"></i>
                      <p>Consultar Proveedor</p>
                    </a>
                  </li>
                </ul>
              </li>
              <!--Reportes-->
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>
                    Reportes
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="/reportes/1" class="nav-link">
                      <i class="far fa-pdf nav-icon"></i>
                      <p>Por Item o Articulo</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/reportes/2" class="nav-link">
                      <i class="far fa-pdf nav-icon"></i>
                      <p>Por Área</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/reportes/3" class="nav-link">
                      <i class="far fa-user nav-icon"></i>
                      <p>Por Usuario</p>
                    </a>
                  </li>
                </ul>
              </li>
              <!--Despachos-->
              <li class="nav-item">
                <a href="/regsalida" class="nav-link">
                  <i class="fas fa-circle nav-icon"></i>
                  <p>Despachos</p>
                </a>
              </li>
              
            </ul>
          </li>
        <?/* } */?>
          <!--Usuario-->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-file-invoice"></i>
              <p>
                Solicitudes
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/nvorequerimento" class="nav-link">
                  <i class="fas fa-list-ul nav-icon"></i>
                  <p>Requerimientos</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fas fa-file-medical-alt nav-icon"></i>
                  <p>Requisiciones</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="/perfil/<?php echo $session->get('userid');?>" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Perfil de usuario
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>