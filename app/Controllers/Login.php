<?php namespace App\Controllers;

use App\Models\Usuarios_model;
use CodeIgniter\API\ResponseTrait;

class Login extends BaseController{

	 use ResponseTrait;

	public function signin(){
		$model = new Usuarios_model();
		$encrypter = \Config\Services::encrypter();
		$session = session();
		if($this->request->isAJAX()){
			$datos = json_decode(base64_decode($this->request->getPost('data')));
			$data['usuemail'] = $datos->usuemail;
			$data['usupass'] = md5($datos->usupass);
			$query = $model->login_user($data);
			if($query->resultID->num_rows > 0){
				$userdata = array();
				foreach($query->getResult() as $row){
					$userdata['userid']   = $row->userid;
					$userdata['usupnom']  = $row->usupnom;
					$userdata['usupape']  = $row->usupape;
					$userdata['usuemail'] = $row->usuemail;
					$userdata['usurol']   = intval($row->idrol);
					$userdata['logged']   = TRUE;
				}
				$session->set($userdata);
				return $this->respond(json_encode(array('message' => 'Usuario encontrado')), 200);
			}
			else{
				return $this->respond(json_encode(array('message' => 'Usuario o contraseña incorrectos')), 404);
			}
		}
		else{
			redirect()->to('/403');
		}
	}

	public function logout(){
		$this->session->destroy();
		return redirect()->to('/');
	}
}