<?php namespace App\Controllers;

use App\Models\Almacen_model;
use CodeIgniter\API\ResponseTrait;

class Almacen extends BaseController{

	use ResponseTrait;

	/*Metodo que carga la vista principal del almacen*/
	public function index(){
		echo $this->loadTemplate('/almacen');
	}

	/*Metodo que carga una tabla con lo que contiene el almacen*/

	public function existencias(){
		$model = new Almacen_model();
		$query = $model->obtenerExistencias();
		$tbody = '';
		if($query->resultID->num_rows > 0){
			foreach ($query->getResult() as $row) {
				if($row->numexis > 5 && $row->numexis < 10){
					$tbody .= '<tr class="table-warning"><td>'.$row->itemid.'</td><td>'.utf8_decode($row->prodmar).'</td><td>'.utf8_decode($row->prodmodel).'</td><td>'.$row->numexis.'</td></tr>';
				}
				else if($row->numexis < 5){
					$tbody .= '<tr class="table-danger"><td>'.$row->itemid.'</td><td>'.utf8_decode($row->prodmar).'</td><td>'.utf8_decode($row->prodmodel).'</td><td>'.$row->numexis.'</td></tr>';
				}
				else{
					$tbody .= '<tr class="table-light"><td>'.$row->itemid.'</td><td>'.utf8_decode($row->prodmar).'</td><td>'.utf8_decode($row->prodmodel).'</td><td>'.$row->numexis.'</td></tr>';	
				}
			}
		}
		else{
			$tbody .= '<tr class="table-light"><td colspan="4" class="text-center">Sin Registros</td>';
		}
		echo view('template/header');
		echo view('template/nav_bar');
		echo view('almacen/existencias/content', array('tbody' => $tbody));
		echo view('template/footer');
		echo view('almacen/existencias/footer');
	}
	/*Metodo que obtiene las salidas del almacen*/
	public function salidas(){
		$model = new Almacen_model();
		$query = $model->obtenerSalidas();
		$tbody = '';
		if($query->resultID->num_rows > 0){
			foreach ($query->getResult() as $row) {
				$tbody .= '
				<tr>
				<td>'.$row->regsalida.'</td>
				<td>'.$this->formatearFecha($row->fechasal).'</td>
				<td><a href="/ordenes/'.$row->numorden.'">'.$row->numorden.'</a></td>
				<td>'.$row->depnom.'</td>
				<td>'.$row->usupnom.' '.$row->usupape.'</td>
				<td><a href="/saldet/'.$row->regsalida.'">Detalles</a></td>
				</tr>';
			}
		}
		else{
			$tbody .= '<tr class="table-light"><td colspan="6" class="text-center">Sin Registros</td>';
		}
		echo view('template/header');
		echo view('template/nav_bar');
		echo view('almacen/salidas/content', array('tbody' => $tbody));
		echo view('template/footer');
		echo view('almacen/existencias/footer');
	}

	/*Metodo que obtiene las entradas del almacen*/
	public function entradas(){
		$model = new Almacen_model();
		$query = $model->obtenerEntradas();
		$tbody = '';
		if($query->resultID->num_rows > 0){
			foreach ($query->getResult() as $row) {
				$tbody .= '
				<tr>
				<td>'.$row->numregent.'</td>
				<td>'.$row->numfac.'</td>
				<td><a href="/detpro/'.$row->provid.'">'.$row->nomprov.'</a></td>
				<td>'.$this->formatearFecha($row->fecfac).'</td>
				<td>'.$this->formatearFecha($row->fecent).'</td>
				<td>'.$row->usupnom.' '.$row->usupape.'</td>
				<td>'.$row->entcoment.'</td>
				<td><a href="/saldet/'.$row->numregent.'">Detalles</a></td>
				</tr>';
			}
		}
		else{
			$tbody .= '<tr class="table-light"><td colspan="7" class="text-center">Sin Registros</td>';
		}
		echo view('template/header');
		echo view('template/nav_bar');
		echo view('almacen/entradas/content', array('tbody' => $tbody));
		echo view('template/footer');
		echo view('almacen/entradas/footer');
	}

	/*Metodo que refresca la tabla en las existencias*/

	public function refrescarTabla(){
		$model = new Almacen_model();
		$query = $model->obtenerExistencias();
		$tbody = '';
		if($query->resultID->num_rows > 0){
			foreach ($query->getResult() as $row) {
				if($row->numexis > 5 && $row->numexis < 10){
					$tbody .= '<tr class="table-warning"><td>'.$row->itemid.'</td><td>'.utf8_decode($row->prodmar).'</td><td>'.utf8_decode($row->prodmodel).'</td><td>'.$row->numexis.'</td></tr>';
				}
				else if($row->numexis < 5){
					$tbody .= '<tr class="table-danger"><td>'.$row->itemid.'</td><td>'.utf8_decode($row->prodmar).'</td><td>'.utf8_decode($row->prodmodel).'</td><td>'.$row->numexis.'</td></tr>';
				}
				else{
					$tbody .= '<tr class="table-light"><td>'.$row->itemid.'</td><td>'.utf8_decode($row->prodmar).'</td><td>'.utf8_decode($row->prodmodel).'</td><td>'.$row->numexis.'</td></tr>';	
				}
			}
		}
		else{
			$tbody .= '<tr class="table-light"><td colspan="4" class="text-center">Sin Registros</td>';
		}
		return $this->respond(json_encode(array('message' => 'Ejecutado exitosamente', 'tbody' => $tbody)), 200);
	}

	/*Metodo que carga la vista para registrar una entrada al almacen*/
	public function registrarEntrada(){
		$model = new Almacen_model();
		echo view('template/header');
		echo view('template/nav_bar');
		echo view('almacen/entradas/registrar_entrada');
		echo view('template/footer');
		echo view('almacen/entradas/footer');
	}
	public function registrarSalida(){
		echo view('template/header');
		echo view('template/nav_bar');
		echo view('almacen/salidas/registrar_salida');
		echo view('template/footer');
		echo view('almacen/salidas/footer');
	}

	/*Metodo que registra una nueva entrada al almacen*/
	public function newEntrada(){
		if($this->request->isAJAX() && $this->session->get('logged')){
			$data = array();
			$datos = json_decode(utf8_decode(base64_decode($this->request->getPost('data'))), TRUE);
			$model = new Almacen_model();
			$datos['usuregent'] = $this->session->get('userid');
			$datos['numregent'] = intval($model->getLastID()) + 1;
			$query = $model->registrarEntrada($datos);
			if($query){
				return $this->respond(array('message' => 'success', 'num_op' => $datos['numregent']), 200);
			}
			else{
				return $this->respond(array('message' => 'error'), 500);
			}
		}
		else{
			return redirect()->to('/403');
		}
	}
	/*Metodo para registrar un detalle de la entrada*/
	public function addDetalle(){
		if($this->request->isAJAX() && $this->session->get('logged')){
			$datos = json_decode(utf8_decode(base64_decode($this->request->getPost('data'))), TRUE);
			$model = new Almacen_model();
			$query = $model->registrarDetalle($datos);
			if($query){
				return $this->respond(array('message' => 'success'), 200);
			}
			else{
				return $this->respond(array('message' => 'error'), 500);
			}
		}
		else{
			return redirect()->to('/403');
		}
	}

	/*Metodo para obtener los detalles de la entrada*/
	public function getDetalles(){
		if($this->request->isAJAX() && $this->session->get('logged')){
			$datos = json_decode(base64_decode($this->request->getPost('data')), TRUE);
			$model = new Almacen_model();
			$query = $model->getDetalles($datos['numregent']);
			$tbody = '';
			if($query->resultID->num_rows > 0){
				foreach ($query->getResult() as $row) {
					$tbody .= '<tr><td>'.utf8_decode(trim($row->prodmar)).'</td><td>'.utf8_decode($row->prodmodel).'</td><td>'.utf8_decode($row->prodpresent).'</td><td>'.$row->costuni.'</td><td>'.$row->numunid.'</td></tr>';
				}
			}
			else{
				$tbody .= '<tr><td colspan="5">Sin registros</td></tr>';
			}
			return $this->respond(array('message' => 'success', 'data' => utf8_decode($tbody)));
		}
		else{
			return redirect()->to('/403');
		}
	}
}