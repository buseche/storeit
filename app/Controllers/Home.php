<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		return view('welcome_message');
	}

	//--------------------------------------------------------------------
	/*Vista del menu principal*/
	public function dashboard(){
		if($this->session->get('logged')){
			echo $this->loadTemplate('/dashboard');
		}
		else{
			return redirect()->to('/');
		}
	}
	/*Vista para los requerimientos*/
	public function requermientos(){
		if($this->session->get('logged')){
			echo $this->loadTemplate('/catalog');
		}
		else{
			return redirect()->to('/');
		}
	}
	/*Vista de Login*/
	public function login(){
		if($this->session->get('logged')){
			echo view('template/header.php');
			echo view('login/content.php');
			echo view('login/footer.php');
		}
	}
	/*Vista de Admin*/
	public function admin(){
		if($this->session->get('logged') && $this->session->get('idrol') == '1'){
			echo $this->loadTemplate('/admin');
		}
		else{
			return redirect()->to('/403');
		}
	}
	/*Vista de recuperacion de contraseña*/
	public function recuperarPass(){
		echo view('template/header.php');
		echo view('login/recuperar_pass.php');
		echo view('login/footer.php'); 
	}
	/*Vista de prohibido*/
	public function forbidden(){
		if($this->session->get('logged')){
			echo view('errors/html/error_403.php');
		}
		else{
			return redirect()->to('/');
		}
	}
	/*Registro de nuevo proveedor*/
	public function newProvider(){
		echo view('proveedores/new_provider');
	}
}
