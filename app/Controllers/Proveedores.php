<?php namespace App\Controllers;

use App\Models\Proveedores_model;
use CodeIgniter\API\ResponseTrait;

class Proveedores extends BaseController{

	use ResponseTrait;

	public function nuevo(){
		if($this->request->isAJAX() && $this->session->get('logged')){
			$datos = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))), TRUE);
			$model = new Proveedores_model();
			$query = $model->nuevoProveedor($datos);
			if($query){
				return $this->respond(array('message' => 'registrado exitosamente', 200 ));
			}
			else{
				return $this->respond(array('message' => 'ha ocurrido un error', 500 ));
			}
		}
		else{
			return redirect()->to('/403');
		}
	}

	public function consulta(){
		$model = new Proveedores_model();
		$query = $model->getAll();
		$tbody = '';
		if($query->resultID->num_rows > 0){
			foreach($query->getResult() as $row){
				$tbody .= '<tr><td>'.$row->idprov.'</td><td>'.$row->numrif.'</td><td>'.$row->nomprov.'</td><td>'.$row->telef1.'</td><td>'.$row->telef2.'</td><td><button class="btn btn-sm detalles btn-light" id="'.$row->idprov.'">Detalles</button><button class="btn btn-sm editar btn-primary" id="'.$row->idprov.'">Editar</button></td></tr>';
			}
		}
		else{
			$tbody .= '<tr><td class="text-center" colspan="6">Sin Registros</td></tr>';
		}
		echo view('proveedores/all_provider', array('tbody' => $tbody));
	}

	public function detalleProveedor(){
		if($this->request->isAJAX()){
			$id = $this->request->getPost('data');
			$model = new Proveedores_model();
			$query = $model->getSingle($id);
			$data = array();
			if($query->resultID->num_rows > 0){
				foreach($query->getResult() as $row){
					$data['idprov']  = $row->idprov;
					$data['nomprov'] = $row->nomprov;
					$data['telef1'] = $row->telef1;
					$data['telef2'] = $row->telef2;
					$data['direccprov'] = $row->direccprov;
					$data['contemail'] = $row->email;
				}
				return $this->respond(array('message' => 'success', 'data' => $data), 200);
			}
			else{
				return $this->respond(array('message' => 'not found'), 404);	
			}
		}
		else{
			return redirect()->to('/403');
		}
	}

	public function buscarProveedor(){
		if($this->request->isAJAX()){
			$datos = json_decode(base64_decode($this->request->getPost('data')), TRUE);
			$model = new Proveedores_model();
			$query = $model->buscarPorRif($datos['rif']);
			if($query->resultID->num_rows > 0){
				return $this->respond(array('message' => 'success', 'data' => $query->getRowArray()), 200);
			}
			else{
				return $this->respond(array('message' => 'not found'), 404);	
			}			
		}
		else{
			return redirect()->to('/403');
		}		
	}
}