<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;
use App\Models\BaseModel;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = [];

	public $validation;

	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		$this->session = \Config\Services::session();
		//Registro del acceso
		if(strlen(base64_decode($this->request->getPost('data'))) < 1){
			
			$this->watson($this->request->getIPAddress(), $this->request->uri->getPath() , NULL ,$this->request->getUserAgent(), date('Y-m-d h:i:s'), $this->session->get('usuemail'));
		}
		else{
			$this->watson($this->request->getIPAddress(), $this->request->uri->getPath() , base64_decode($this->request->getPost('data')) ,$this->request->getUserAgent(), date('Y-m-d h:i:s'), $this->session->get('usuemail'));	
		}
	}
	/* Metodo que permite la carga del template para los index */
	/*Metodo para llamar la vista de un modulo*/
	public function loadTemplate($module){
		echo view('template/header');
		if($module != '/login'){ 
			echo view('template/nav_bar');
		}
		echo view($module.'/content');
		echo view('template/footer');
		echo view($module.'/footer');
	}
	/**
	 * Metodo que permite registrar los movimientos de la app
	 * por parte del usuario, esto permite un control mas exhaustivo
	 * de la app
	 *
	 * @var access_ip : String => La IP de acceso del usuario
	 * @var url_request : String => El recurso (Controlador) que intenta acceder
	 * @var data_request : String => Datos Via JSON (Envio de formularios) en caso de darse, puede ser NULL
	 * @var user_agent : String => El navegador que utiliza el usuario
	 * @var access_date: TIMESTAMP => Fecha y hora de acceso
	 * @var user_access: String => Usuario que solicita el recurso
	 */

	public function watson(String $access_ip, String $url_request, String $data_request = NULL, String $user_agent, $access_date, String $user_access = NULL){
		$model = new BaseModel();
		$query = $model->recordLog(array(
			'access_ip' => $access_ip,
			'url_request' => $url_request,
			'data_request' => $data_request,
			'user_agent' => $user_agent,
			'access_date'=> $access_date,
			'user_access'=> $user_access
		));
		if($query){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	/*Funcion que formatea fechas*/
    public function formatearFecha($fecha){
        $date1 = explode('-',$fecha);
        $date2 = $date1[2]."-".$date1[1]."-".$date1[0];
        return $date2;
    }

}
