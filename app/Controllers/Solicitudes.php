<?php 
namespace App\Controllers;

use App\Models\Almacen_model;
use App\Models\Solicitudes_model;
use CodeIgniter\API\ResponseTrait;


class Solicitudes extends BaseController{

	use ResponseTrait;

	//Metodo para cargar el catalogo de existencias
	public function catalog(){
		$model = new Almacen_model();
		$query = $model->obtenerExistencias();
		$tbody = '';
		if($query->resultID->num_rows > 0){
			foreach($query->getResult() as $row){
				$tbody .= '
				<tr>
					<td>
					<img style="max-width: 4rem; max-height: 4rem;" src="'.$row->imgref.'" class="h-8">
					</td>
	                <td>
	                '.$row->prodmar.'
	                </td>
	                <td class="text-left text-muted d-none d-md-table-cell text-nowrap">
	                	'.$row->prodmodel.'
	                </td>
	                <td class="text-right">
	                	<button id="'.$row->codbar.'" class="btn btn-sm btn-info add-detail"><i class="fas fa-plus"></i>Añadir</button>
	                </td>
                </tr>';
			}
		}
		else{
			$tbody .= '<tr><td colspan="3">Sin Registros</td></tr>';
		}
		return $tbody;
	}


	//Metodo para cargar la vista de los requermientos*/
	public function requerimientos(){
		if($this->session->get('logged')){
			//Solicitamos un nuevo numero de orden
			$model = new Solicitudes_model();
			$numorden = $model->getNumOrden();
			$query = $model->addNumOrden(array(
				'numorden' => $numorden,
				'fecsol'   => date('Y-m-d'),
				'ordstatus' => 4,
				'usureg'    => $this->session->get('userid')
			));
			unset($model);
			$tbody = $this->catalog();
			//Armamos la vista

			echo view('template/header');
			echo view('template/nav_bar');
			echo view('catalog/newreq', array('tbody' => $tbody, 'numorden' => $numorden));
			echo view('template/footer');
			echo view('catalog/footer');
		}
		else{
			return redirect()->to('/');
		}
	}

	//Metodo que añade elementos en las presolicitudes
	public function addDetalle(){
		$model = new Solicitudes_model();
		if($this->request->isAJAX() && $this->session->get('logged')){
			$datos = json_decode(utf8_decode(base64_decode($this->request->getPost('data'))) ,TRUE);
			//Validamos que el item no haya sido añadido previamente
			if($model->itemExists($datos['codbar'], $datos['numorden'])){
				$query = $model->updateItem($datos);
				if($query){
					return $this->respond(array('message' => 'Item Actualizado exitosamente', 200));
				}
				else{
					return $this->respond(array('message' => 'Error al añadir el item', 500));	
				}
			}
			else{
				$query = $model->addItem($datos);
				if($query){
					return $this->respond(array('message' => 'Item Añadido exitosamente'), 200);
				}
				else{
					return $this->respond(array('message' => 'Error al añadir el item'), 500);
				}
			}
		}
		else{
			return redirect()->to('/');
		}
		
	}

	/*Metodo para listar el historico de prerequerimientos*/

	public function obtenerPreordenes(){
		if($this->request->isAJAX() && $this->session->get('logged')){
			$model = new Solicitudes_model();
			$query = $model->getPreordenes($this->session->get('userid'));
			$tbody = '';
			//Mostramos las preordenes
			if($query->resultID->num_rows > 0){
				foreach ($query->getResult() as $row) {
					$tbody .= '<tr>
					<td>'.$row->numorden.'</td>
					<td>'.$this->formatearFecha($row->fecsol).'</td>
					<td>'.$row->statusnom.'</td>';
					if($row->ordstatus == '4'){
						$tbody .= '<td><a href="/preordenesdetalle/'.$row->numorden.'">Detalle</a></td>';
					}
					else{
						$tbody .= '<td><a href="/verpreorden/'.$row->numorden.'">Detalle</a></td>';	
					}
					$tbody .= '</tr>';
				}
			}
			else{
				$tbody .= '<tr><td colspan="4" class="text-center">Sin registros</td></tr>';
			}
			return $this->respond(array('message' => 'success', 'data' => $tbody), 200);
		}
		else{
			return redirect()->to('/');
		}
	}

	//Metodo que muestra la tabla de las ordenes solicitadas

	public function obtenerOrdenes(){
		if($this->request->isAJAX() && $this->session->get('logged')){
			$model = new Solicitudes_model();
			$query = $model->getOrdenes($this->session->get('userid'));
			$tbody = '';
			//Mostramos las preordenes
			if($query->resultID->num_rows > 0){
				foreach ($query->getResult() as $row) {
					$tbody .= '<tr>
					<td>'.$row->numorden.'</td>
					<td>'.$this->formatearFecha($row->fecaprob).'</td>
					<td>'.$row->statusnom.'</td>
					<td><a href="/ordendetalle/'.$row->numorden.'">Detalle</a></td>
					</tr>';
				}
			}
			else{
				$tbody .= '<tr><td colspan="4" class="text-center">Sin registros</td></tr>';
			}
			return $this->respond(array('message' => 'success', 'data' => $tbody), 200);
		}
		else{
			return redirect()->to('/');
		}
	}

	public function preordenesdetalle($id = NULL){
		if($this->session->get('logged')){
			$model = new Solicitudes_model();
			$query = $model->detallePreorden($id);
			$tbody = '';
			if($query->resultID->num_rows > 0){
				foreach($query->getResult() as $row){
					$tbody .= '
					<tr>
					<td class="text-center">'.$row->detalleid.'</td>
					<td class="text-center">'.$row->prodmar.'</td>
					<td class="text-center">'.$row->prodmodel.'</td>
					<td class="text-center">'.$row->numuni.'</td>
					<td><button class="btn btn-sm btn-danger eliminar" id="'.$row->detalleid.'"><i class="fas fa-trash"></i>Eliminar</button></td>
					</tr>';
				}
			}
			else{
				$tbody .= '
				<tr>
				<td colspan="5" class="text-center">Esta solicitud esta vacia, añade items con el botón Añadir</td>
				</tr>
				';
			}

			//Cargamos la vista 

			echo view('template/header');
			echo view('template/nav_bar');
			echo view('catalog/detpreorder', array('tbody' => $tbody, 'numorden' => $id));
			echo view('template/footer');
			echo view('catalog/footer');
		}
		else{
			return redirect()->to('/');
		}
	}

	//Metodo para añadir nuevos items a una solicitud ya creada

	public function editarPreorden($id = NULL){
		if($this->session->get('logged')){
			$tbody = $this->catalog();
			echo view('template/header');
			echo view('template/nav_bar');
			echo view('catalog/newreq', array('tbody' => $tbody, 'numorden' => $id));
			echo view('template/footer');
			echo view('catalog/footer');
		}
		else{
			return redirect()->to('/');
		}
	}

	//Metodo para eliminar la preorden

	public function eliminarPreorden($id = NULL){
		if($this->session->get('logged')){
			$model = new Solicitudes_model();
			$query = $model->deletePreorden($id);
			if($query){
				return redirect()->to('/inicio');
			}
			else{
				echo view('errors/html/production');
			}
		}
		else{
			return redirect()->to('/');
		}
	}

	//Metodo para eliminar items de la preorden 

	public function eliminarItemPreOrden(){
		if($this->request->isAJAX() && $this->session->get('logged')){
			$model = new Solicitudes_model();
			$tbody = '';
			$datos = json_decode(base64_decode($this->request->getPost('data')), TRUE);
			$query = $model->deleteItemPreorden($datos['itemid']);
			if($query){
				$query = $model->detallePreorden($datos['numorden']);
				if($query->resultID->num_rows > 0){
					foreach($query->getResult() as $row){
						$tbody .= '
						<tr>
						<td class="text-center">'.$row->detalleid.'</td>
						<td class="text-center">'.$row->prodmar.'</td>
						<td class="text-center">'.$row->prodmodel.'</td>
						<td class="text-center">'.$row->numuni.'</td>
						<td><button class="btn btn-sm btn-danger eliminar" id="'.$row->detalleid.'"><i class="fas fa-trash"></i>Eliminar</button></td>
						</tr>';
					}
				}
				else{
					$tbody .= '
					<tr>
					<td colspan="5" class="text-center">Esta solicitud esta vacia, añade items con el botón Añadir</td>
					</tr>
					';
				}
				return $this->respond(array('message' => 'success', 'data' => $tbody), 200);
			}
			else{
				return $this->respond(array('message' => 'error'), 500);
			}
		}
		else{
			return redirect()->to('/');
		}
	}

	//Metodo para aprobar las preordenes
	public function aprobarPreorden($id = NULL){
		if($this->session->get('logged')){
			$model = new Solicitudes_model();
			$query = $model->confirmarPreorden($id);
			if($query){
				return redirect()->to('/inicio');
			}
		}
		else{
			return redirect()->to('/');
		}

	}

	//Metodo para ver las preordenes en Tramite

	public function verPreorden($id = NULL){
		if($this->session->get('logged')){
			$model = new Solicitudes_model();
			$tbody = '';
			$tabla = $model->detallePreorden($id);
			$userdata = array();
			//Detalles de la preorden
			if($tabla->resultID->num_rows > 0){
				foreach($tabla->getResult() as $row){
					$tbody .= '
					<tr>
					<td>'.$row->detalleid.'</td>
					<td>'.$row->prodmar.'</td>
					<td>'.$row->prodmodel.'</td>
					<td>'.$row->numuni.'</td>
					</tr>';
				}
			}
			else{
				$tbody .= '
				<tr>
				<td colspan="4" class="text-center">Sin Registros</td>
				</tr>
				';
			}
			$userdata['tbody'] = $tbody;
			//Datos del solicitante
			$datos = $model->getDatosPreorden($id);
			if($datos->resultID->num_rows > 0){
				foreach($datos->getResult() as $row){
					$userdata['numorden'] = $row->numorden;
					$userdata['fecsol'] = $this->formatearFecha($row->fecsol);
					$userdata['usupnom'] = $row->usupnom;
					$userdata['usupape'] = $row->usupape;
					$userdata['depnom'] = $row->depnom;
					$userdata['dirnom'] = $row->dirnom;
				}
			}

			echo view('template/header');
			echo view('template/nav_bar');
			echo view('catalog/verpreorden', $userdata);
			echo view('template/footer');
			echo view('catalog/footer');
		}
		else{
			return redirect()->to('/');
		}
	}

	/*

	Metodos para las ordenes

	*/


}