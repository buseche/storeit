<?php namespace App\Controllers;

require_once APPPATH.'/ThirdParty/dompdf/autoload.inc.php';

use Dompdf\Dompdf;


class Reportes extends BaseController{

	public function index(){
		$dompdf = new Dompdf();
		$dompdf->loadHtml('hello world');
		// (Optional) Setup the paper size and orientation
		$dompdf->setPaper('A4', 'landscape');
		// Render the HTML as PDF
		$dompdf->render();
		// Output the generated PDF to Browser
		$dompdf->stream();
	}

}