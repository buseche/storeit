<?php
namespace App\Models;

class Proveedores_model extends BaseModel{

	public function getAll(){
		$builder = $this->dbconn('sta_proveedores');
		$query = $builder->get();
		return $query;
	}

	public function nuevoProveedor(Array $data){
		$builder = $this->dbconn('sta_proveedores');
		$query = $builder->insert($data);
		return $query;
	}

	public function getSingle(String $id){
		$builder = $this->dbconn('sta_proveedores');
		$query = $builder->getWhere(['idprov' => $id]);
		return $query;
	}

	public function buscarPorRif(String $RIF){
		$builder = $this->dbconn('sta_proveedores');
		$query = $builder->getWhere(['numrif' => $RIF]);
		return $query;
	}
}