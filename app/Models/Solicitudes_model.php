<?php
namespace App\Models;

class Solicitudes_model extends BaseModel{


	/*


	Metodos para cargar las preordenes del sistema

	*/

	//Metodo para obtener el numero de orden
	public function getNumOrden(){
		$builder = $this->dbconn('sta_preordenes');
		$query = $builder->get();
		if($this->preordenExists($query->resultID->num_rows)){
			return intval($query->resultID->num_rows) + 2;
		}
		else{
			return intval($query->resultID->num_rows) + 1;
		}
		
		
	}

	//Validacion para la preorden, si existe el indice
	public function preordenExists($numorden){
		$builder = $this->dbconn('sta_preordenes');
		$builder->where('numorden', $numorden);
		$query = $builder->get();
		if($query->resultID->num_rows > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	//Metodo para añaidr el numero de orden
	public function addNumOrden(Array $data){
		$builder = $this->dbconn('sta_preordenes');
		$query = $builder->insert($data);
		return $query;
	}

	public function itemExists(String $codbar, String $numorden){
		$builder = $this->dbconn('sta_detalles_preordenes');
		$builder->where('codbar', $codbar);
		$builder->where('numorden', $numorden);
		$query = $builder->get();
		if($query->resultID->num_rows > 0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	/*
	Metodo para actualizar el item si ya existe
	*/
	public function updateItem(Array $data){
		$builder = $this->dbconn('sta_detalles_preordenes');
		
		$builder->where('codbar',  $data['codbar']);
		
		$builder->where('numorden', $data['numorden']);
		
		$query = $builder->get()->getRowArray();
		
		$numuni = intval($query['numuni']);
		
		$builder->resetQuery();
		
		$builder->where('codbar', $data['codbar']);
		
		$builder->where('numorden', $data['numorden']);
		
		$query = $builder->update(array('numuni' => ($numuni + intval($data['numuni']))));
		
		return $query;
	}
	//Metodo que inserta un nuevo item
	public function addItem(Array $data){
		$builder = $this->dbconn('sta_detalles_preordenes');
		$query = $builder->insert($data);
		return $query;
	}

	//Metodo para obtener las preordenes

	public function getPreordenes($user){
		$builder = $this->dbconn('sta_preordenes a');
		$builder->select('a.numorden, a.fecsol, a.ordstatus ,b.statusnom, c.usupnom, c.usupape');
		$builder->join('sta_status b', 'a.ordstatus = b.statusid');
		$builder->join('sta_usuarios c', 'a.usureg = c.userid');
		$builder->where('a.usureg', $user);
		$query = $builder->get();
		return $query;
	}

	
	//Metodo para obtener los detalles de las preordenes

	public function detallePreorden($id){
		$builder = $this->dbconn('sta_detalles_preordenes a');
		$builder->select('a.detalleid, b.prodmar, b.prodmodel, a.numuni');
		$builder->join('sta_productos b' , 'a.codbar = b.codbar');
		$builder->where('a.numorden', $id);
		$query = $builder->get();
		return $query;
	}

	//Metodo para eliminar las Preordenes

	public function deletePreorden($id){
		$builder = $this->dbconn('sta_preordenes');
		$query = $builder->delete(['numorden' => $id]);
		return $query;
	}

	//Metodo para eliminar los detalles de las preordenes
	public function deleteItemPreorden($id){
		$builder = $this->dbconn('sta_detalles_preordenes');
		$query = $builder->delete(['detalleid' => $id]);
		return $query;
	}

	//Metodo para cambiar las preordenes a estatus en Tramite

	public function confirmarPreorden($id){
		$builder = $this->dbconn('sta_preordenes');
		$builder->where('numorden', $id);
		$query = $builder->update(['ordstatus' => '1']);
		return $query;
	}

	//Metodo para traer los detalles de una preorden

	public function getDatosPreorden($id){
		$builder = $this->dbconn('sta_preordenes a');
		$builder->select('a.numorden, a.fecsol, b.usupnom, b.usupape, c.depnom, e.dirnom');
		$builder->join('sta_usuarios b', 'a.usureg = b.userid');
		$builder->join('sta_departamentos c', 'b.deptid = c.deptid');
		$builder->join('sta_dep_dir d', 'c.deptid = d.depid');
		$builder->join('sta_direcciones e', 'd.dirid = e.dirid');
		$builder->where('a.numorden', $id);
		$query = $builder->get();
		return $query;
	}

	/*
	

	Metodos para las ordenes del sistema
	

	*/

	//Metodo para obtener las ordenes ya aprobadas

	public function getOrdenes($user){
		$builder = $this->dbconn('sta_ordenes a');
		$builder->select('a.numorden, a.fecaprob, b.statusnom, c.usupnom, c.usupape');
		$builder->join('sta_status b', 'a.statusid = b.statusid');
		$builder->join('sta_usuarios c', 'a.usuaprob = c.userid');
		$builder->where('a.ususol', $user);
		$query = $builder->get();
		return $query;
	}

	

}