<?php 
namespace App\Models;

class Almacen_model extends BaseModel{

	/*Metodo que obtiene las existencias dentro del almacen*/

	public function obtenerExistencias(){
		$builder = $this->dbconn('sta_existencias a');
		$builder->select('a.itemid, a.numexis, b.prodmar, b.prodmodel');
		$builder->join('sta_productos b', 'a.codbar = b.codbar');
		$query = $builder->get();
		return $query;
	}

	/*Metodo que obtiene las salidas dentro del almacen*/

	public function obtenerSalidas(){
		$builder = $this->dbconn('sta_almacen_salidas a');
		$builder->select('a.salidaid , a.fecsal, a.numorden, b.depnom, c.usupnom , c.usupape, a.commsal');
		$builder->join('sta_departamentos b', 'a.depdest = b.deptid');
		$builder->join('sta_usuarios c' , 'a.usureg = c.userid');
		$query = $builder->get();
		return $query;
	}

	/*Metodo que obtiene todas las entradas dentro del almacen*/

	public function obtenerEntradas(){
		$builder = $this->dbconn('sta_almacen_entradas a');
		$builder->select('a.numregent, a.numfac , a.provid , b.nomprov, a.fecfac, a.fecent, a.entcoment, c.usupnom, c.usupape');
		$builder->join('sta_proveedores b', 'a.provid = b.idprov');
		$builder->join('sta_usuarios c', 'a.usuregent = c.userid');
		$query = $builder->get();
		return $query;
	}

	/*Metodo que obtiene el ultimo id de registro*/
	public function getLastID(){
		$builder = $this->dbconn('sta_almacen_entradas');
		$builder->selectCount('numregent');
		$query = $builder->get();
		$id = $query->getRow()->numregent;
		return $id;
	}
	/*Metodo que registra la entrada del almacen*/
	public function registrarEntrada(Array $data){
		/*Registramos la entrada*/
		$builder = $this->dbconn('sta_almacen_entradas');
		$query = $builder->insert($data);
		return $query;
	}
	/*Metodo que registra del detalle de la entrada*/
	public function registrarDetalle(Array $data){
		$builder = $this->dbconn('sta_entradas_detalles');
		$builder->insert($data);
		//Actualizamos las existencias
		$query = $this->aumentaExistencias($data['codbar'], intval($data['numunid']));
		return $query;
	}
	/*Metodo que obtiene todas las entradas segun la operacion*/
	public function getDetalles(String $numregent){
		$builder = $this->dbconn('sta_entradas_detalles a');
		$builder->select('b.prodmar, b.prodmodel, a.prodpresent, a.numunid, a.costuni');
		$builder->join('sta_productos b', 'a.codbar = b.codbar');
		$builder->where('regent' , $numregent);
		$query = $builder->get();
		return $query;
	}
}