<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::login');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}

/*Rutas de la aplicacion*/
$routes->get('/admin', 'Home::admin');
$routes->get('/403', 'Home::forbidden');
$routes->get('/inicio', 'Home::dashboard');
$routes->get('/logout','Login::logout');

/*Rutas para login*/
$routes->post('/signin', 'Login::signin');

/*Rutas para usar con AJAX*/
$routes->post('/counters', 'ControlPanel::getStats');
$routes->post('/getAllData', 'ControlPanel::get_all_data');


/*Rutas del panel de control*/

$routes->resource('direcciones');
$routes->resource('departamentos');
$routes->resource('usuarios');
$routes->resource('roles');
$routes->get('/getDepts/(:num)' , 'Usuarios::getDeptDetalles/$1');
$routes->get('/adduser', 'Usuarios::addUser');
$routes->get('/edituser/(:num)', 'Usuarios::editUser/$1');

/*Rutas para el almacen */
$routes->get('/almacen', 'Almacen::index');
$routes->get('/existencias', 'Almacen::existencias');
$routes->get('/entradas', 'Almacen::entradas');
$routes->get('/regentrada', 'Almacen::registrarEntrada');
$routes->get('/regsalida', 'Almacen::registrarSalida');
$routes->get('/salidas', 'Almacen::salidas');
$routes->post('/acttabla','Almacen::refrescarTabla');
$routes->post('/addentrada','Almacen::newEntrada');
$routes->post('/adddetalle', 'Almacen::addDetalle');
$routes->post('/detalles', 'Almacen::getDetalles');

/*Rutas para los productos*/
$routes->get('/regprod', 'Productos::registrar');
$routes->get('/consultaproducto', 'Productos::consutaProducto');
$routes->post('/addproduct', 'Productos::addProducto');
$routes->get('/editarproducto/(:num)', 'Productos::show/$1');
$routes->post('/buscarxcodbar', 'Productos::searchByCodbar');

/*Rutas para los proveedores*/
$routes->post('/nuevoproveedor', 'Proveedores::nuevo');
$routes->get('/consultaproveedor', 'Proveedores::consulta');
$routes->post('/detalleproveedor', 'Proveedores::detalleProveedor');
$routes->post('/buscarproveedor', 'Proveedores::buscarProveedor');
$routes->get('/newprovider' , 'Home::newProvider');

/*Rutas para las solicitudes*/
$routes->get('/nvorequerimento', 'Solicitudes::requerimientos');
$routes->post('/addpreorddet', 'Solicitudes::addDetalle');
$routes->post('/obtpreordenes' , 'Solicitudes::obtenerPreordenes');
$routes->post('/obtordenes', 'Solicitudes::obtenerOrdenes');
$routes->get('/preordenesdetalle/(:num)', 'Solicitudes::preordenesdetalle/$1');
$routes->get('/ordendetalle/(:num)', 'Solicitudes::detalleOrden/$1');
$routes->get('/editarpreorden/(:num)', "Solicitudes::editarPreorden/$1");
$routes->get('/eliminarpreorden/(:num)', "Solicitudes::eliminarPreorden/$1");
$routes->post('/eliminaritem', 'Solicitudes::eliminarItemPreOrden');
$routes->get('/confirmarpreorden/(:num)', "Solicitudes::aprobarPreorden/$1");
$routes->get('/verpreorden/(:num)', "Solicitudes::verPreorden/$1");